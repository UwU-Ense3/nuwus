import 'package:ense3/UI/EventsTab/EventsListAndFilter.dart';
import 'package:ense3/authentication/ui/auth_builder.dart';
import 'package:ense3/bloc/events_list_bloc/event_list_bloc.dart';
import 'package:ense3/bloc/events_list_bloc/visibility_filter.dart';
import 'package:ense3/event_creator/form/ui/event_form.dart';
import 'package:ense3/models/global.dart';
import 'package:ense3/ressources/Event.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AllEvents extends StatefulWidget {
  const AllEvents({Key? key}) : super(key: key);

  @override
  State<AllEvents> createState() => _AllEventsState();
}

class _AllEventsState extends State<AllEvents> {
  final _searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<EventsListBloc, EventsListState>(
      listener: (context, state) {
        if (state is EventsListLoadSuccess) {
          if (!state.filter.isSameQuery(_searchController.text)) {
            _searchController.text = state.filter.searchQuery;
          }
        } else {
          _searchController.text = '';
        }
      },
      child: WillPopScope(
        onWillPop: () {
          BlocProvider.of<EventsListBloc>(context).add(FilterUpdateRequested(
              searchQuery: '', showPast: false, selectedTags: const []));
          BlocProvider.of<EventsListBloc>(context)
              .add(SortingUpdateRequested(sortOrder: SortOrder.chronological));
          return Future.value(true);
        },
        child: GestureDetector(
          onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
          child: Scaffold(
            appBar: AppBar(
              systemOverlayStyle: SystemUiOverlayStyle.light,
              leading: IconButton(
                icon: const Icon(Icons.arrow_back_rounded),
                onPressed: () {
                  BlocProvider.of<EventsListBloc>(context).add(
                      FilterUpdateRequested(
                          searchQuery: '',
                          showPast: false,
                          selectedTags: const []));
                  BlocProvider.of<EventsListBloc>(context).add(
                      SortingUpdateRequested(
                          sortOrder: SortOrder.chronological));
                  Navigator.pop(context);
                },
              ),
              actions: [
                Expanded(
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 64),
                      child: TextField(
                        controller: _searchController,
                        style: TextStyle(
                            color: Theme.of(context).colorScheme.onPrimary),
                        cursorColor: Theme.of(context).colorScheme.onPrimary,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white.withOpacity(0.12),
                          enabledBorder: const OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            borderSide: BorderSide.none,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            borderSide: BorderSide(
                              color: Colors.grey.withOpacity(0.5),
                              width: 1,
                              style: BorderStyle.solid,
                            ),
                          ),
                          border: const OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            borderSide: BorderSide.none,
                          ),
                          isDense: true,
                          contentPadding: const EdgeInsets.all(0),
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          labelText:
                              AppLocalizations.of(context)!.chercher_event,
                          suffixIcon: IconButton(
                            icon: const Icon(Icons.clear_rounded),
                            color: Theme.of(context).colorScheme.onPrimary,
                            onPressed: () {
                              BlocProvider.of<EventsListBloc>(context)
                                  .add(FilterUpdateRequested(searchQuery: ''));
                              TextInputAction.done;
                            },
                          ),
                          prefixIcon: Icon(
                            Icons.search_rounded,
                            color: Theme.of(context).colorScheme.onPrimary,
                          ),
                          labelStyle: TextStyle(
                              color: Theme.of(context).colorScheme.onPrimary),
                        ),
                        onChanged: (query) =>
                            BlocProvider.of<EventsListBloc>(context)
                                .add(FilterUpdateRequested(searchQuery: query)),
                      ),
                    ),
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.filter_list_rounded),
                  onPressed: () {
                    showCustomModalSheet(context);
                  },
                ),
              ],
            ),
            body: EventsListAndFilter(
              bloc: BlocProvider.of<EventsListBloc>(context),
            ),
            floatingActionButton: FloatingActionButton(
              tooltip: AppLocalizations.of(context)!.addAnEvent,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const AuthBuilder(child: EventForm()),
                  ),
                );
              },
              child: Icon(
                Icons.add,
                color: Theme.of(context).colorScheme.onPrimary,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Padding buildSortTile(
    BuildContext context, {
    required SortOrder currentSortOrder,
    required SortOrder sortOrder,
    required SortOrder reverseSortOrder,
    required String name,
  }) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 4),
      child: Container(
        decoration: BoxDecoration(
          color: currentSortOrder == sortOrder ||
                  currentSortOrder == reverseSortOrder
              ? Theme.of(context).primaryColor
              : Theme.of(context).backgroundColor,
          borderRadius: const BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        child: ListTile(
          trailing: Icon(
            currentSortOrder == sortOrder
                ? Icons.arrow_downward_rounded
                : Icons.arrow_upward_rounded,
            color: currentSortOrder == sortOrder ||
                    currentSortOrder == reverseSortOrder
                ? Theme.of(context).colorScheme.onPrimary
                : Colors.transparent,
          ),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
          ),
          title: Text(
            name,
            style: Theme.of(context).textTheme.headline2!.copyWith(
                  fontWeight: FontWeight.normal,
                  color: currentSortOrder == sortOrder ||
                          currentSortOrder == reverseSortOrder
                      ? Theme.of(context).colorScheme.onPrimary
                      : Theme.of(context).textTheme.headline2!.color,
                ),
          ),
          onTap: () {
            if (currentSortOrder == sortOrder) {
              BlocProvider.of<EventsListBloc>(context)
                  .add(SortingUpdateRequested(sortOrder: reverseSortOrder));
            } else {
              BlocProvider.of<EventsListBloc>(context)
                  .add(SortingUpdateRequested(sortOrder: sortOrder));
            }
            // Navigator.pop(context);
          },
        ),
      ),
    );
  }

  void showCustomModalSheet(BuildContext context) {
    showModalBottomSheet<void>(
      context: context,
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10), topRight: Radius.circular(10)),
      ),
      builder: (BuildContext context) {
        return BlocBuilder<EventsListBloc, EventsListState>(
          builder: (context, state) {
            SortOrder currentSortOrder = SortOrder.chronological;
            if (state is EventsListLoadSuccess) {
              currentSortOrder = state.filter.sortOrder;
            }
            return Wrap(
              children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10)),
                    color: Theme.of(context).backgroundColor,
                  ),
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      ChoiceChip(
                        shape: StadiumBorder(
                            side: BorderSide(
                                color: Theme.of(context).primaryColor)),
                        selectedColor: Theme.of(context).primaryColor,
                        backgroundColor: Theme.of(context).backgroundColor,
                        label: Text(
                          AppLocalizations.of(context)!.montrer_events_passe,
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(
                                  color: (state is EventsListLoadSuccess
                                          ? state.filter.showPast
                                          : false)
                                      ? Colors.white
                                      : Theme.of(context).primaryColor),
                        ),
                        selected: state is EventsListLoadSuccess
                            ? state.filter.showPast
                            : false,
                        onSelected: (bool selected) {
                          BlocProvider.of<EventsListBloc>(context)
                              .add(FilterUpdateRequested(showPast: selected));
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 12, top: 8),
                        child: Text(
                          AppLocalizations.of(context)!.trier_par,
                          style:
                              Theme.of(context).textTheme.headline2!.copyWith(
                                    fontWeight: FontWeight.w700,
                                  ),
                        ),
                      ),
                      buildSortTile(
                        context,
                        name: AppLocalizations.of(context)!.date_heure,
                        currentSortOrder: currentSortOrder,
                        sortOrder: SortOrder.chronological,
                        reverseSortOrder: SortOrder.reverseChronological,
                      ),
                      buildSortTile(
                        context,
                        name: AppLocalizations.of(context)!.nom,
                        currentSortOrder: currentSortOrder,
                        sortOrder: SortOrder.alphabetical,
                        reverseSortOrder: SortOrder.reverseAlphabetical,
                      ),
                      buildSortTile(
                        context,
                        name: AppLocalizations.of(context)!.prix,
                        currentSortOrder: currentSortOrder,
                        sortOrder: SortOrder.increasingPrice,
                        reverseSortOrder: SortOrder.decreasingPrice,
                      ),
                      // Padding(
                      //   padding: const EdgeInsets.only(bottom: 12),
                      //   child: Text(
                      //     AppLocalizations.of(context).filtrer_par,
                      //     style: Theme.of(context).textTheme.headline2.copyWith(
                      //           fontWeight: FontWeight.w700,
                      //         ),
                      //   ),
                      // ),
                      // state is EventsListLoadSuccess
                      //     ? Wrap(
                      //         children: List.generate(
                      //           state.tagsList.length,
                      //           (i) {
                      //             Tag tag = state.tagsList[i];
                      //             return Padding(
                      //               padding: const EdgeInsets.symmetric(horizontal: 4),
                      //               child: TagChoiceChip(
                      //                 tag: tag,
                      //                 selected: state.filter.selectedTags.contains(tag),
                      //                 onSelected: (bool newSelected) {
                      //                   if (newSelected) {
                      //                     BlocProvider.of<EventsListBloc>(context).add(
                      //                         FilterUpdateRequested(
                      //                             selectedTags: state.filter.selectedTags
                      //                               ..add(tag)));
                      //                   } else {
                      //                     BlocProvider.of<EventsListBloc>(context).add(
                      //                         FilterUpdateRequested(
                      //                             selectedTags: state.filter.selectedTags
                      //                               ..remove(tag)));
                      //                   }
                      //                 },
                      //               ),
                      //             );
                      //           },
                      //         ),
                      //       )
                      //     : CircularProgressIndicator(),
                    ],
                  ),
                ),
              ],
            );
          },
        );
      },
    );
  }
}

class TagChoiceChip extends StatefulWidget {
  final Tag tag;
  final bool selected;
  final void Function(bool) onSelected;

  const TagChoiceChip({
    Key? key,
    required this.tag,
    required this.selected,
    required this.onSelected,
  }) : super(key: key);

  @override
  State<TagChoiceChip> createState() => _TagChoiceChipState();
}

class _TagChoiceChipState extends State<TagChoiceChip> {
  @override
  Widget build(BuildContext context) {
    var color = HexColor.fromHex(widget.tag.color);
    return ChoiceChip(
      shape: StadiumBorder(side: BorderSide(color: color)),
      selectedColor: color,
      selected: widget.selected,
      onSelected: widget.onSelected,
      label: Text(
        widget.tag.name,
        style: Theme.of(context)
            .textTheme
            .bodyText1!
            .copyWith(color: widget.selected ? Colors.white : color),
      ),
      backgroundColor: Theme.of(context).backgroundColor,
      // shadowColor:
      //     Theme.of(context).textTheme.bodyText1.color.withOpacity(0.5),
    );
  }
}
