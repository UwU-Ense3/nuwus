import 'package:dart_date/dart_date.dart';
import 'package:ense3/UI/CustomWidgets/CustomCard.dart';
import 'package:ense3/UI/EventsTab/event_view_page.dart';
import 'package:ense3/bloc/events_list_bloc/event_list_bloc.dart';
import 'package:ense3/models/global.dart';
import 'package:ense3/ressources/Event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'event_view_body.dart';

class DayView extends StatelessWidget {
  final int daysToAdd;
  late final DateTime date;

  DayView({Key? key, required this.daysToAdd}) : super(key: key) {
    date = DateTime.now().add(Duration(days: daysToAdd));
  }

  Widget _buildTextDayNb(context, int id) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 4.0),
      child: Text(
        relativeDate(date, AppLocalizations.of(context)),
        style: TextStyle(
          fontFamily: 'Roboto',
          fontSize: 15,
          fontWeight: FontWeight.bold,
          color: Theme.of(context).textTheme.bodyText1!.color,
        ),
      ),
    );
  }

  Widget _buildEventListForDay(BuildContext context) {
    return BlocBuilder<EventsListBloc, EventsListState>(
      builder: (BuildContext context, state) {
        // En cas d'erreur : si pas de connexion
        if (state is EventsListHasError) {
          return Container();
        }

        // Si pas encore de donnée dans le Future : indicateur de chargement
        if (state is EventsListLoadInProgress) {
          return Container();
        }

        var eventsList = (state as EventsListLoadSuccess).eventsList;

        // Si il n'y a pas d'événements dans la liste
        if (eventsList.isEmpty) {
          return Container();
        }
        List<Event> eventsOfDay = eventsList.where((e) => e.beginTime.isSameDay(date)).toList();
        return ListView.separated(
          itemCount: eventsOfDay.length,
          itemBuilder: (context, int i) => Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListTile(
              // mainAxisAlignment: MainAxisAlignment.spaceAround,
              // crossAxisAlignment: CrossAxisAlignment.center,
              // children: [
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => EventViewPage(event: eventsOfDay[i]),
                  ),
                );
              },
              title: Text(
                eventsOfDay.elementAt(i).name,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontFamily: 'Roboto',
                  fontWeight: FontWeight.w600,
                  fontSize: 16,
                ),
              ),
              subtitle: Text(
                DateFormat('HH:mm', 'fr_FR').format(eventsOfDay.elementAt(i).beginTime),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Theme.of(context).textTheme.bodyText1!.color,
                  fontFamily: 'Roboto',
                  fontWeight: FontWeight.normal,
                  fontSize: 14,
                ),
              ),
            ),
          ),
          separatorBuilder: (context, int i) => Divider(
            height: 0,
            thickness: 1,
            color: Theme.of(context).dividerColor,
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EventsListBloc, EventsListState>(
      builder: (BuildContext context, state) {
        // En cas d'erreur : si pas de connexion
        if (state is EventsListHasError) {
          return Container();
        }

        // Si pas encore de donnée dans le Future : indicateur de chargement
        if (state is EventsListLoadInProgress) {
          return Container();
        }
        var eventsList = (state as EventsListLoadSuccess).eventsList;

        // Si il n'y a pas d'événements dans la liste
        if (eventsList.isEmpty) {
          return Container();
        }
        List<Event> eventsOfTheDay = eventsList.where((e) => e.beginTime.isSameDay(date)).toList();
        if (eventsOfTheDay.isNotEmpty) {
          // Si il y a au moins un événement dans la journée
          return Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 4.0),
                child: _buildTextDayNb(context, daysToAdd),
              ),
              Expanded(
                child: CustomCard(
                  child: _buildEventListForDay(context),
                ),
              ),
            ],
          );
        } else {
          // Sinon, si il n'y a pas d'événement dans la journée
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: _buildTextDayNb(context, daysToAdd),
              ),
              ConstrainedBox(
                constraints:
                    const BoxConstraints.tightForFinite(width: 40, height: 40),
                child: Container(
                  width: 30,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Theme.of(context).cardColor,
                    border: Border.all(color: Theme.of(context).dividerColor),
                  ),
                ),
              ),
            ],
          );
        }
      },
    );
  }
}

class ColumnBuilder extends StatelessWidget {
  final IndexedWidgetBuilder itemBuilder;
  final MainAxisAlignment mainAxisAlignment;
  final MainAxisSize mainAxisSize;
  final CrossAxisAlignment crossAxisAlignment;
  final TextDirection? textDirection;
  final VerticalDirection verticalDirection;
  final int itemCount;

  const ColumnBuilder({
    Key? key,
    required this.itemBuilder,
    required this.itemCount,
    this.mainAxisAlignment = MainAxisAlignment.start,
    this.mainAxisSize = MainAxisSize.max,
    this.crossAxisAlignment = CrossAxisAlignment.center,
    this.textDirection,
    this.verticalDirection = VerticalDirection.down,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(itemCount, (index) => itemBuilder(context, index))
          .toList(),
    );
  }
}
