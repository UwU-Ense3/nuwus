import 'package:ense3/UI/EventsTab/event_view_body.dart';
import 'package:ense3/app_config.dart';
import 'package:ense3/authentication/login_bloc/LoginBloc.dart';
import 'package:ense3/authentication/ui/auth_builder.dart';
import 'package:ense3/event_creator/form/ui/event_form.dart';
import 'package:ense3/models/global.dart';
import 'package:ense3/repository/EventRepository.dart';
import 'package:ense3/ressources/Event.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class EventViewPage extends StatelessWidget {
  final Event event;

  EventViewPage({Key? key, required this.event}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle.light,
        title: Text(event.name),
        actions: [
          BlocBuilder<LoginBloc, LoginState>(
            builder: (context, state) => (state is LoggedIn &&
                    (state.user.id == event.createdById ||
                        state.user.canModifyAllEvents))
                ? IconButton(
                    icon: const Icon(Icons.edit_rounded),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => AuthBuilder(
                            child: EventForm(
                              event: event,
                              editMode: true,
                            ),
                          ),
                        ),
                      );
                    },
                  )
                : Container(),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 5.0),
            child: IconButton(
              onPressed: () {
                snackBar(context, Icons.auto_awesome,
                    AppLocalizations.of(context)!.availableSoon);
              },
              icon: Icon(
                Icons.notifications,
                color: Theme.of(context).colorScheme.onPrimary,
                size: 24,
              ),
            ),
          )
        ],
      ),
      body: EventViewBody(event: event),
    );
  }
}

void fetchAndShowEvent(BuildContext context, int eventId) {
  Navigator.push(
    context,
    MaterialPageRoute(
      builder: (context) => FutureBuilder(
        future: EventRepository(
                apiUrl: context
                    .findAncestorWidgetOfExactType<AppConfig>()!
                    .apiBaseUrl)
            .fetchEvent(eventId),
        builder: (context, AsyncSnapshot<Event> snapshot) {
          if (snapshot.hasData) {
            return EventViewPage(event: snapshot.data!);
          } else {
            return Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
        },
      ),
    ),
  );
}
