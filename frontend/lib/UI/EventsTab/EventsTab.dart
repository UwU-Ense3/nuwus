import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:ense3/UI/CustomWidgets/CustomCard.dart';
import 'package:ense3/UI/EventsTab/AllEvents.dart';
import 'package:ense3/UI/EventsTab/EventsList.dart';
import 'package:ense3/bloc/events_list_bloc/event_list_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'Calendar.dart';

class EventsTab extends StatefulWidget {
  const EventsTab({Key? key}) : super(key: key);

  @override
  State<EventsTab> createState() => _EventsTabState();
}

class _EventsTabState extends State<EventsTab> {
  @override
  void initState() {
    super.initState();
    initializeDateFormatting('fr');
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: Stack(
        alignment: Alignment.bottomRight,
        children: [
          Opacity(
            opacity: 0.5,
            child: Container(
              padding: const EdgeInsets.only(left: 50),
              child: Theme.of(context).brightness == Brightness.dark
                  ? const Image(
                      image: AssetImage('lib/assets/uwu_chan.png'),
                      fit: BoxFit.cover,
                    )
                  : Container(),
            ),
          ),
          Column(
            children: [
              Flexible(
                fit: FlexFit.loose,
                flex: 5,
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: CustomCard(
                    child: EventsList(
                      bloc: BlocProvider.of<EventsListBloc>(context),
                      hideHeader: false,
                      hideDividers: false,
                      hideDescription: true,
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    ),
                  ),
                ),
              ),
              const Flexible(
                fit: FlexFit.loose,
                flex: 4,
                child: HorizontalCalendar(),
              ),
              Flexible(
                fit: FlexFit.loose,
                flex: 1,
                child: ElevatedButton(
                  style: Theme.of(context).outlinedButtonTheme.style,
                  child: Text(
                    AppLocalizations.of(context)!.tous_events,
                    style: TextStyle(
                        color: Theme.of(context).colorScheme.onPrimary),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const AllEvents()),
                    );
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
