import 'package:ense3/UI/EventsTab/event_view_page.dart';
import 'package:ense3/authentication/login_bloc/LoginBloc.dart';
import 'package:ense3/authentication/ui/auth_builder.dart';
import 'package:ense3/bloc/events_list_bloc/event_list_bloc.dart';
import 'package:ense3/event_creator/form/ui/event_form.dart';
import 'package:ense3/models/global.dart';
import 'package:ense3/ressources/Event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:url_launcher/url_launcher.dart';

class EventsList extends StatelessWidget {
  final EventsListBloc bloc;
  final bool hideHeader;
  final bool hideDividers;
  final bool hideDescription;
  final EdgeInsets padding;
  final bool showEditOptions;

  const EventsList({
    Key? key,
    this.hideHeader = false,
    this.hideDividers = false,
    this.hideDescription = false,
    this.padding = const EdgeInsets.all(8),
    this.showEditOptions = false,
    required this.bloc,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EventsListBloc, EventsListState>(
      bloc: bloc,
      builder: (context, EventsListState state) {
        if (state is EventsListInitial) {
          bloc.add(ListUpdateRequested());
        }
        // En cas d'erreur : c'est à dire si pas de connexion.
        // On check connectionState pour ne pas afficher 2 fois la
        // snackbar
        if (state is EventsListHasError) {
          // addPostFrameCallback permet de faire une action après la
          // construction du widget
          WidgetsBinding.instance.addPostFrameCallback((_) {
            snackBar(context, Icons.wifi_off_rounded,
                AppLocalizations.of(context)!.erreur_connexion_serveur);
            Logger().d('Erreur de connexion au serveur');
          });
          return LayoutBuilder(
            builder: (context, constraints) {
              return RefreshIndicator(
                color: Theme.of(context).primaryColor,
                onRefresh: () async {
                  bloc.add(ListUpdateRequested());
                },
                child: Padding(
                  padding: padding,
                  child: SingleChildScrollView(
                    physics: const AlwaysScrollableScrollPhysics(),
                    child: ConstrainedBox(
                      constraints: BoxConstraints(
                        minHeight: constraints.maxHeight,
                        minWidth: constraints.maxWidth,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              AppLocalizations.of(context)!.erreur_connexion,
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                          ),
                          OutlinedButton(
                            onPressed: () async {
                              bloc.add(ListUpdateRequested());
                            },
                            style: Theme.of(context).outlinedButtonTheme.style,
                            child: Text(
                              AppLocalizations.of(context)!.reessayer,
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            },
          );
        }

        if (state is EventsListLoadSuccess) {
          return RefreshIndicator(
            color: Theme.of(context).primaryColor,
            onRefresh: () async => bloc.add(ListUpdateRequested()),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: padding.vertical),
              child: ListView.separated(
                separatorBuilder: (BuildContext context, int i) => hideDividers
                    ? Container()
                    : Divider(
                        height: 0,
                        thickness: 1,
                        color: Theme.of(context).dividerColor,
                      ),
                // physics: BouncingScrollPhysics(),
                itemCount: state.eventsList.length + (hideHeader ? 0 : 2),
                itemBuilder: (BuildContext context, int i) {
                  if (i == 0 && !hideHeader) {
                    return Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: 8, horizontal: padding.horizontal),
                      child: Text(
                        AppLocalizations.of(context)!.events_a_venir,
                        style: Theme.of(context).textTheme.headline2!.copyWith(
                            color:
                                Theme.of(context).textTheme.bodyText1!.color),
                      ),
                    );
                  } else if (i == 1 && !hideHeader) {
                    return Divider(
                      height: 0,
                      thickness: 1,
                      color: Theme.of(context).dividerColor,
                    );
                  } else {
                    if (!hideHeader) {
                      i -= 2;
                    }
                    return Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: padding.horizontal),
                      child: GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        child: EventContainer(
                          state.eventsList[i].id,
                          state.eventsList,
                          hideDescription: hideDescription,
                          showEditOptions: showEditOptions,
                        ),
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) =>
                                  EventViewPage(event: state.eventsList[i]),
                            ),
                          );
                          context
                              .read<EventsListBloc>()
                              .add(EventViewed(state.eventsList[i].id));
                        },
                      ),
                    );
                  }
                },
              ),
            ),
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}

class EventContainer extends StatefulWidget {
  final List<Event> eventList;
  final int id;
  final bool hideDescription;
  final bool showEditOptions;

  const EventContainer(
    this.id,
    this.eventList, {
    Key? key,
    this.hideDescription = false,
    this.showEditOptions = false,
  }) : super(key: key);

  @override
  State<EventContainer> createState() => _EventContainerState();
}

class _EventContainerState extends State<EventContainer> {
  // bool _participates;

  @override
  Widget build(BuildContext context) {
    var event =
        widget.eventList.firstWhere((element) => element.id == widget.id);
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(
            top: 12,
            bottom: 12,
            right: 15,
            left: 0,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                DateFormat('dd').format(event.beginTime),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Theme.of(context).textTheme.bodyText1!.color,
                  fontSize: 40,
                  height: 0.99,
                  fontFamily: 'Roboto',
                  fontWeight: FontWeight.w300,
                ),
              ),
              Text(
                DateFormat('MMM', AppLocalizations.of(context)!.localeName)
                    .format(event.beginTime)
                    .toUpperCase(),
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Theme.of(context).textTheme.bodyText1!.color,
                  fontSize: 14.2,
                  height: 0.8,
                  fontFamily: 'Roboto',
                  fontWeight: FontWeight.normal,
                ),
              ),
            ],
          ),
        ),
        Flexible(
          child: Padding(
            padding: const EdgeInsets.only(
              top: 12,
              bottom: 12,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  event.name,
                  textAlign: TextAlign.left,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 17,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  event.formatOrganizers(),
                  textAlign: TextAlign.left,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  style: TextStyle(
                    color: Theme.of(context).textTheme.bodyText1!.color,
                    fontSize: 15,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.normal,
                  ),
                ),
                Row(
                  children: [
                    Icon(
                      Icons.location_on,
                      color: Theme.of(context).textTheme.bodyText1!.color,
                      size: 20,
                    ),
                    Expanded(
                      child: Text(
                        event.location,
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                        style: TextStyle(
                          color: Theme.of(context).textTheme.bodyText1!.color,
                          fontSize: 15,
                          fontFamily: 'Roboto',
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ),
                  ],
                ),
                widget.hideDescription
                    ? Container()
                    : Padding(
                        padding: const EdgeInsets.only(top: 8),
                        child: MarkdownBody(
                          onTapLink: (text, link, title) async {
                            await launchUrl(Uri.parse(link ?? ''),
                                mode: LaunchMode.externalApplication);
                          },
                          data: event.description.length >
                                  120 // todo : à améliorer pour mettre une hauteur maximale
                              ? '${event.description.replaceAll('\n', '\n\n').substring(0, 120)}...'
                              : event.description.replaceAll('\n', '\n\n'),
                          styleSheet: getMarkdownStyleSheet(context),
                        ),
                        // Text(
                        //   event.description,
                        //   style: Theme.of(context).textTheme.bodyText1,
                        // ),
                      )
              ],
            ),
          ),
        ),
        widget.showEditOptions
            ? BlocBuilder<LoginBloc, LoginState>(
                builder: (context, state) {
                  return (state is LoggedIn &&
                          (state.user.id == event.createdById ||
                              state.user.canModifyAllEvents))
                      ? IconButton(
                          icon: Icon(
                            Icons.edit_rounded,
                            size: 24,
                            color: Theme.of(context).textTheme.bodyText1!.color,
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => AuthBuilder(
                                    child: EventForm(
                                  event: event,
                                  editMode: true,
                                )),
                              ),
                            );
                          },
                        )
                      : Container();
                },
              )
            : Container(),
      ],
    );
  }
}
