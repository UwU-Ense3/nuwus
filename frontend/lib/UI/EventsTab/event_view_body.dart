import 'dart:typed_data';

import 'package:ense3/UI/CustomWidgets/CustomCard.dart';
import 'package:ense3/models/global.dart';
import 'package:ense3/ressources/Event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

class EventViewBody extends StatelessWidget {
  final Event event;
  final bool isLocalImage;

  const EventViewBody(
      {Key? key, required this.event, this.isLocalImage = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          Container(
            height: 250,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              borderRadius:
                  const BorderRadius.vertical(bottom: Radius.circular(4)),
              boxShadow: [
                BoxShadow(
                  offset: const Offset(1, 1),
                  blurRadius: 5,
                  color: Colors.black.withOpacity(0.25),
                ),
              ],
              color: Theme.of(context).colorScheme.secondary,
            ),
            child: ClipRRect(
              borderRadius:
                  const BorderRadius.vertical(bottom: Radius.circular(4)),
              child: Hero(
                tag: 'imageHero',
                child: isLocalImage
                    ? Image.memory(
                        event.imageBytes ?? Uint8List(0),
                        fit: BoxFit.cover,
                        errorBuilder: (context, object, stackTrace) => Icon(
                          Icons.image_not_supported_rounded,
                          color: Theme.of(context)
                              .colorScheme
                              .onPrimary
                              .withOpacity(0.2),
                          size: 150,
                        ),
                      )
                    : event.imageLink != null && event.imageLink != ''
                        ? Image.network(
                            event.imageLink ?? '',
                            fit: BoxFit.cover,
                            errorBuilder: (context, object, stackTrace) => Icon(
                              Icons.image_not_supported_rounded,
                              color: Theme.of(context)
                                  .colorScheme
                                  .onPrimary
                                  .withOpacity(0.2),
                              size: 150,
                            ),
                          )
                        : Icon(
                            Icons.image_not_supported_rounded,
                            color: Theme.of(context)
                                .colorScheme
                                .onPrimary
                                .withOpacity(0.2),
                            size: 150,
                          ),
              ),
            ),
          ),
          ListView(
            children: [
              GestureDetector(
                onTap: () =>
                    Navigator.push(context, MaterialPageRoute(builder: (_) {
                  return ImageView(
                    url: event.imageLink ?? '',
                    bytes: event.imageBytes,
                  );
                })),
                child: Container(
                  height: 180,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.transparent,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  left: 12.0,
                  right: 12,
                  top: 0,
                  bottom: 12,
                ),
                child: CustomCard(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 16, right: 20, left: 20, bottom: 0),
                        child: Text(
                          event.formatFullDatetime(AppLocalizations.of(context),
                              full: true),
                          style: Theme.of(context).textTheme.headline1,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 7, right: 20, left: 20, bottom: 0),
                        child: Text(
                          event.name,
                          style: Theme.of(context)
                              .textTheme
                              .bodyText2!
                              .copyWith(fontWeight: FontWeight.w600),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 3, right: 20, left: 20, bottom: 0),
                        child: Row(
                          children: [
                            Icon(
                              Icons.location_on,
                              color:
                                  Theme.of(context).textTheme.bodyText1!.color,
                              size: 20,
                            ),
                            Expanded(
                              child: Text(
                                event.location,
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 20, right: 20, left: 20, bottom: 0),
                        child: SizedBox(
                          height: 35,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Row(
                                children: [
                                  const Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 5.0),
                                    child: Icon(
                                      Icons.attach_money_rounded,
                                      size: 25,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                    child: Text(
                                      (event.price == 0 || event.price == null)
                                          ? '-'
                                          : NumberFormat.currency(
                                                  symbol: '€',
                                                  locale: AppLocalizations.of(
                                                          context)!
                                                      .localeName)
                                              .format(event.price! / 100),
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText2!
                                          .copyWith(
                                              fontWeight: FontWeight.w300),
                                    ),
                                  ),
                                ],
                              ),
                              VerticalDivider(
                                width: 0,
                                color: Theme.of(context).dividerColor,
                                thickness: 1,
                              ),
                              Row(
                                children: [
                                  const Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 5.0),
                                    child: Icon(
                                      Icons.people_rounded,
                                      size: 25,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                    child: Text(
                                      (event.numberParticipants == 0 ||
                                              event.numberParticipants == null)
                                          ? '-'
                                          : NumberFormat.compact()
                                              .format(event.numberParticipants),
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText2!
                                          .copyWith(
                                              fontWeight: FontWeight.w300),
                                    ),
                                  ),
                                ],
                              ),
                              VerticalDivider(
                                width: 0,
                                color: Theme.of(context).dividerColor,
                                thickness: 1,
                              ),
                              Row(
                                children: [
                                  const Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 5.0),
                                    child: Icon(
                                      Icons.calendar_today,
                                      size: 25,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                    child: Text(
                                      DateFormat('dd/MM')
                                          .format(event.beginTime),
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText2!
                                          .copyWith(
                                              fontWeight: FontWeight.w300),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 20, right: 20, top: 15, bottom: 0),
                        child: Text(
                            AppLocalizations.of(context)!.organise_par +
                                event.formatOrganizers(),
                            overflow: TextOverflow.fade,
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(fontWeight: FontWeight.w600)),
                      ),
                      event.numberViews != null
                          ? Padding(
                              padding: const EdgeInsets.only(
                                  left: 20, right: 20, top: 5, bottom: 0),
                              child: Text(
                                  event.numberViews.toString() +
                                      ' ' +
                                      AppLocalizations.of(context)!.vues,
                                  style: Theme.of(context).textTheme.bodyText1),
                            )
                          : Container(),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 20, right: 20, top: 23, bottom: 0),
                        child: MarkdownBody(
                          onTapLink: (text, link, title) async {
                            await launchUrl(Uri.parse(link ?? ''),
                                mode: LaunchMode.externalApplication);
                          },
                          data: event.description.replaceAll('\n', '\n\n'),
                          styleSheet: getMarkdownStyleSheet(context),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 20, right: 20, top: 20, bottom: 3),
                        child: Text(AppLocalizations.of(context)!.tags,
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1!
                                .copyWith(fontWeight: FontWeight.w900)),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 20, right: 20, top: 0, bottom: 20),
                        child: SizedBox(
                          height: 40,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: event.tags.length,
                            itemBuilder: (context, int i) => Padding(
                              padding: const EdgeInsets.only(right: 8),
                              child: Chip(
                                labelPadding: const EdgeInsets.all(2.0),
                                label: Text(event.tags[i].name,
                                    style:
                                        Theme.of(context).textTheme.bodyText1),
                                backgroundColor:
                                    Theme.of(context).backgroundColor,
                                elevation: 3.0,
                                // shadowColor:
                                //     Theme.of(context).textTheme.bodyText1.color.withOpacity(0.5),
                                padding: const EdgeInsets.all(8.0),
                              ),
                            ),
                          ),
                        ),
                      ),
                      event.createdTime != null && event.createdBy != null
                          ? Padding(
                              padding: const EdgeInsets.only(
                                  left: 20, right: 20, top: 0, bottom: 10),
                              child: Text(
                                AppLocalizations.of(context)!.addedOnAndBy(
                                    DateFormat('dd/MM/yyyy')
                                        .format(event.createdTime!),
                                    event.createdBy!),
                                style: Theme.of(context)
                                    .textTheme
                                    .labelSmall!
                                    .copyWith(color: Colors.grey),
                              ),
                            )
                          : Container(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

/// Vue de l'image d'illustration en plein écran
class ImageView extends StatelessWidget {
  final String url;
  final Uint8List? bytes;

  const ImageView({Key? key, required this.url, this.bytes}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Center(
          child: Hero(
            tag: 'imageHero',
            child: bytes == null
                ? Image.network(
                    url,
                    fit: BoxFit.cover,
                    errorBuilder: (context, object, stackTrace) => Icon(
                      Icons.image_not_supported_rounded,
                      color: Theme.of(context)
                          .colorScheme
                          .onPrimary
                          .withOpacity(0.2),
                      size: 150,
                    ),
                  )
                : Image.memory(
                    bytes!,
                    fit: BoxFit.cover,
                    errorBuilder: (context, object, stackTrace) => Icon(
                      Icons.image_not_supported_rounded,
                      color: Theme.of(context)
                          .colorScheme
                          .onPrimary
                          .withOpacity(0.2),
                      size: 150,
                    ),
                  ),
          ),
        ),
      ),
      onTap: () {
        Navigator.pop(context);
      },
    );
  }
}
