import 'package:ense3/UI/EventsTab/EventsListAndFilter.dart';
import 'package:ense3/bloc/events_list_bloc/event_list_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SearchEvent extends SearchDelegate {

  final EventsListBloc bloc;

  SearchEvent({required this.bloc});

  @override
  ThemeData appBarTheme(BuildContext context) {
    return super.appBarTheme(context).copyWith(
          appBarTheme: AppBarTheme(
            color: Theme.of(context).primaryColor,
            systemOverlayStyle: SystemUiOverlayStyle.light,
            titleTextStyle: TextStyle(
              color: Theme.of(context).colorScheme.onPrimary,
              fontSize: 18,
            ),
          ),
        );
  }

  @override
  String get searchFieldLabel =>
      'Chercher événement'; //TODO: comment avoir accès à la traduction ici ?

  @override
  TextStyle get searchFieldStyle => TextStyle(
        color: Colors.white.withOpacity(0.5),
        fontSize: 18,
      );

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        bloc.add(FilterUpdateRequested(searchQuery: ''));
        return Future.value(true);
      },
      child: IconButton(
        icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow,
          progress: transitionAnimation,
        ),
        onPressed: () {
          bloc.add(FilterUpdateRequested(searchQuery: ''));
          Navigator.pop(context);
        },
      ),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    bloc.add(FilterUpdateRequested(searchQuery: query));
    return EventsListAndFilter(
      bloc: bloc,
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return buildResults(context);
  }
}
