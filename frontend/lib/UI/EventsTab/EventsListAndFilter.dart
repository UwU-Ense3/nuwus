import 'package:ense3/bloc/events_list_bloc/event_list_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'EventsList.dart';

class EventsListAndFilter extends StatelessWidget {
  final EventsListBloc bloc;

  const EventsListAndFilter({Key? key, required this.bloc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        BlocBuilder<EventsListBloc, EventsListState>(
            bloc: bloc,
            builder: (BuildContext context, EventsListState state) {
              return Padding(
                padding: const EdgeInsets.only(top: 8.0, left: 20),
                child: ChoiceChip(
                  shape: StadiumBorder(side: BorderSide(color: Theme.of(context).primaryColor)),
                  selectedColor: Theme.of(context).primaryColor,
                  backgroundColor: Theme.of(context).backgroundColor,
                  label: Text(
                    AppLocalizations.of(context)!.montrer_events_passe,
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        color: (state is EventsListLoadSuccess
                            ? state.filter.showPast
                            : false)
                            ? Colors.white
                            : Theme.of(context).primaryColor),
                  ),
                  selected: state is EventsListLoadSuccess ? state.filter.showPast : false,
                  onSelected: (bool selected) {
                    bloc.add(FilterUpdateRequested(showPast: selected));
                  },
                ),
              );
            }),
        Expanded(
          child: EventsList(
            bloc: bloc,
            hideDividers: true,
            hideHeader: true,
            padding: const EdgeInsets.symmetric(horizontal: 8),
            showEditOptions: true,
          ),
        ),
      ],
    );
  }
}
