import 'dart:math';

import 'package:dart_date/dart_date.dart';
import 'package:ense3/bloc/events_list_bloc/event_list_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'DayView.dart';

class HorizontalCalendar extends StatefulWidget {
  const HorizontalCalendar({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _HorizontalCalendarState();
}

class _HorizontalCalendarState extends State<HorizontalCalendar> {
  static const int _nbDaysShown = 15;

  @override
  Widget build(BuildContext context) {
    double halfWidth = (MediaQuery.of(context).size.width) / 2;
    return Container(
      alignment: const Alignment(0, 0),
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: BlocBuilder<EventsListBloc, EventsListState>(
          builder: (BuildContext context, state) {
        // En cas d'erreur : si pas de connexion
        if (state is EventsListHasError) {
          return Opacity(
            opacity: 0.1,
            child: Icon(
              Icons.wifi_off_rounded,
              size: 150,
              color: Theme.of(context).textTheme.bodyText1!.color,
            ),
          );
        }

        // Si pas encore de donnée dans le Future : indicateur de chargement
        if (state is EventsListLoadInProgress || state is EventsListInitial) {
          return const CircularProgressIndicator();
        }

        if (state is EventsListLoadSuccess) {
          var eventsList = state.eventsList;

          // Si il n'y a pas d'événements dans la liste
          if (eventsList.isEmpty) {
            return Text(
              AppLocalizations.of(context)!.pas_evenement_programme,
              style: Theme.of(context).textTheme.bodyText1,
            );
          }
          // Si les événements sont trop éloignés
          if (eventsList.first.beginTime.isAfter(DateTime.now()
              .endOfDay
              .add(const Duration(days: _nbDaysShown - 1)))) {
            return Text(
              AppLocalizations.of(context)!.pas_evenement_programme_sem,
              style: Theme.of(context).textTheme.bodyText1,
              textAlign: TextAlign.center,
            );
          }

          // Sinon affichage du calendrier
          return CalendarListView(
            nbDaysShown: _nbDaysShown,
            halfWidth: halfWidth,
          );
        } else {
          // Impossible d'arriver ici
          return Text(AppLocalizations.of(context)!.erreur);
        }
      }),
    );
  }
}

class CalendarListView extends StatefulWidget {
  CalendarListView({
    Key? key,
    required int nbDaysShown,
    required this.halfWidth,
  })  : _nbDaysShown = nbDaysShown,
        scaleValueMax = pow(halfWidth * 1.5, 2),
        super(key: key);

  final int _nbDaysShown;
  final double halfWidth;
  final scaleValueMax;

  final int itemExtent = 150;

  @override
  State<CalendarListView> createState() => _CalendarListViewState();
}

class _CalendarListViewState extends State<CalendarListView> {
  final ScrollController _controller =
      ScrollController(initialScrollOffset: 35);
  double scrollOffset = 35.0;

  @override
  void initState() {
    super.initState();
    _controller.addListener(() {
      setState(() {
        scrollOffset = _controller.offset; // Position de scroll en pixels
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemExtent: widget.itemExtent.toDouble(),
      physics: const BouncingScrollPhysics(),
      scrollDirection: Axis.horizontal,
      itemCount: widget._nbDaysShown + 1,
      itemBuilder: (context, id) {
        if (id == 0) {
          // Empty container to center the first day in the horizontal calendar
          return Container();
        }
        id -= 1;
        // Calculate the value for the scaling of container id based on scroll
        // position
        // double scale = 1 -
        //     ((scrollOffset + widget.halfWidth - 150 * (id + 1.5)) / 500).abs();
        double scale = 1 -
            pow(
                    scrollOffset +
                        widget.halfWidth -
                        widget.itemExtent * (id + 1.5),
                    2) /
                widget.scaleValueMax;
        if (scale < 0) {
          scale = 0;
        }
        // return SizedBox(height: scale, child: Container(color: Colors.greenAccent,));},
        return Transform(
          // Transform : scale le container
          transform: Matrix4.identity()..scale(scale),
          alignment: Alignment.center,
          child: Opacity(
            // Opacity : gère la transparence (disparition en fondu)
            opacity: scale,
            child: DayView(
              daysToAdd: id,
            ),
          ),
        );
      },
      controller: _controller,
    );
  }
}
