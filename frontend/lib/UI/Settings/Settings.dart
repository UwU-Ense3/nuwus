import 'package:ense3/authentication/login_bloc/LoginBloc.dart';
import 'package:ense3/authentication/ui/auth_builder.dart';
import 'package:ense3/bloc/localization_cubit/localization_cubit.dart';
import 'package:ense3/models/global.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:logger/logger.dart';
import 'package:theme_mode_handler/theme_mode_handler.dart';
import 'package:url_launcher/url_launcher.dart';

class Settings extends StatelessWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle.light,
        title: Text(
          AppLocalizations.of(context)!.parametres,
        ),
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
//            padding: EdgeInsets.all(20),
            title: Text(
              AppLocalizations.of(context)!.theme,
              style: Theme.of(context).textTheme.bodyText2,
            ),
            subtitle: Text(
              ThemeModeHandler.of(context)!.themeMode == ThemeMode.light
                  ? 'Light Ense3'
                  : 'Dark UwU',
              style: const TextStyle(
                fontFamily: 'Roboto',
                color: Color(0xff808080),
                fontSize: 13,
              ),
            ),
            onTap: () {
              showModalBottomSheet<void>(
                context: context,
                isScrollControlled: true,
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10)),
                ),
                builder: (BuildContext context) {
                  return Wrap(
                    children: [
                      Container(
                        color: Theme.of(context).backgroundColor,
                        padding: const EdgeInsets.all(10),
                        child: Column(
                          children: <Widget>[
                            ListTile(
                              title: Text(
                                  AppLocalizations.of(context)!.theme_light,
                                  style: Theme.of(context).textTheme.bodyText2),
                              onTap: () {
                                ThemeModeHandler.of(context)!
                                    .saveThemeMode(ThemeMode.light);
                                Logger().d('Switching to light theme');
                                Navigator.pop(context);
                              },
                            ),
                            ListTile(
                              title: Text(
                                  AppLocalizations.of(context)!.theme_dark,
                                  style: Theme.of(context).textTheme.bodyText2),
                              onTap: () {
                                ThemeModeHandler.of(context)!
                                    .saveThemeMode(ThemeMode.dark);
                                Logger().d('Switching to dark theme');
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                },
              );
            },
          ),
          ListTile(
            title: Text(
              AppLocalizations.of(context)!.langue,
              style: Theme.of(context).textTheme.bodyText2,
            ),
            subtitle: Text(
              AppLocalizations.of(context)!.langue_selct,
              style: const TextStyle(
                fontFamily: 'Roboto',
                color: Color(0xff808080),
                fontSize: 13,
              ),
            ),
            onTap: () {
              showModalBottomSheet<void>(
                context: context,
                isScrollControlled: true,
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10)),
                ),
                builder: (BuildContext context) {
                  return Wrap(
                    children: [
                      Container(
                        color: Theme.of(context).backgroundColor,
                        padding: const EdgeInsets.all(10),
                        child: Column(
                          children: <Widget>[
                            ListTile(
                              title: Text('Français',
                                  style: Theme.of(context).textTheme.bodyText2),
                              onTap: () {
                                BlocProvider.of<LocalizationCubit>(context)
                                    .switchTo(const Locale('fr'));
                                // ThemeModeHandler.of(context).saveThemeMode(ThemeModeHandler.of(context).themeMode); // Utilisé pour mettre à jour le contexte
                                Logger().d('Switching to french');
                                Navigator.pop(context);
                              },
                            ),
                            ListTile(
                              title: Text('English',
                                  style: Theme.of(context).textTheme.bodyText2),
                              onTap: () {
                                BlocProvider.of<LocalizationCubit>(context)
                                    .switchTo(const Locale('en'));
                                // ThemeModeHandler.of(context).saveThemeMode(ThemeModeHandler.of(context).themeMode); // Utilisé pour mettre à jour le contexte
                                Logger().d('Switching to english');
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                },
              );
            },
          ),
          BlocBuilder<LoginBloc, LoginState>(
            builder: (context, state) {
              return ListTile(
                title: Text(
                    state is LoggedIn
                        ? AppLocalizations.of(context)!.logOut
                        : AppLocalizations.of(context)!.logIn,
                    style: Theme.of(context).textTheme.bodyText2),
                onTap: () {
                  if (state is LoggedIn) {
                    context.read<LoginBloc>().add(UserLogout());
                  } else {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AuthBuilder(
                          child: Scaffold(
                            body: Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(20.0),
                                    child: Text(
                                      AppLocalizations.of(context)!
                                          .youAreConnected,
                                      style:
                                          Theme.of(context).textTheme.bodyText2,
                                    ),
                                  ),
                                  ElevatedButton(
                                    child: Text(
                                      AppLocalizations.of(context)!.retour,
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1!
                                          .copyWith(
                                              color: Theme.of(context)
                                                  .colorScheme
                                                  .onPrimary),
                                    ),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  }
                },
                subtitle: state is LoggedIn
                    ? Text(
                        AppLocalizations.of(context)!.loggedInAs(
                            state.user.firstName, state.user.lastName),
                        style: const TextStyle(
                          fontFamily: 'Roboto',
                          color: Color(0xff808080),
                          fontSize: 13,
                        ),
                      )
                    : null,
              );
            },
          ),
          ListTile(
            title: Text(
              AppLocalizations.of(context)!.signal_bug,
              style: Theme.of(context).textTheme.bodyText2,
            ),
            onTap: () {
              var uri = Uri(
                scheme: 'mailto',
                path: 'nuwus@2uwu4u.club',
                query: 'subject=Bug report pour NUwUs',
              );
              launchUrl(uri, mode: LaunchMode.externalApplication);
            },
          ),
          ListTile(
            title: Text(
              AppLocalizations.of(context)!.about,
              style: Theme.of(context).textTheme.bodyText2,
            ),
            subtitle: Text(
              AppLocalizations.of(context)!.credit_contact_info,
              style: const TextStyle(
                fontFamily: 'Roboto',
                color: Color(0xff808080),
                fontSize: 13,
              ),
            ),
            onTap: () {
              showModalBottomSheet<void>(
                context: context,
                isScrollControlled: true,
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10)),
                ),
                builder: (BuildContext context) {
                  return Wrap(
                    children: [
                      Container(
                        color: Theme.of(context).backgroundColor,
                        padding: const EdgeInsets.all(10),
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20, right: 20, left: 20, bottom: 0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  const SizedBox(
                                    height: 60,
                                    child: Image(
                                      image:
                                          AssetImage('lib/assets/uwu_logo.png'),
                                      fit: BoxFit.fitHeight,
                                    ),
                                  ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        AppLocalizations.of(context)!
                                            .develope_par,
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText2!
                                            .copyWith(fontSize: 18),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 20, right: 20, left: 20, bottom: 20),
                              child: MarkdownBody(
                                data:
                                    AppLocalizations.of(context)!.source_code +
                                        AppLocalizations.of(context)!.contact +
                                        AppLocalizations.of(context)!.discord,
                                styleSheet: MarkdownStyleSheet(
                                  p: Theme.of(context).textTheme.bodyText1,
                                  a: Theme.of(context)
                                      .textTheme
                                      .bodyText1!
                                      .copyWith(color: Colors.blueAccent),
                                ),
                                onTapLink: (text, link, title) async {
                                  await launchUrl(Uri.parse(link ?? ''),
                                      mode: LaunchMode.externalApplication);
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                },
              );
            },
            onLongPress: () {
              snackBar(context, Icons.auto_fix_high, '🤔');
            },
          ),
        ],
      ),
    );
  }
}
