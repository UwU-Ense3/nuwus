import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:ense3/UI/EventsTab/EventsTab.dart';
import 'package:ense3/UI/Settings/Settings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Onglets commentés pour l'instant, à décommenter quand on aura implémenté l'intranet et l'emploi du temps

    //  return DefaultTabController(
    // length: 3,
    // child:
    return Scaffold(
      // drawer: Drawer(
      //   child: Column(
      //     mainAxisAlignment: MainAxisAlignment.spaceAround,
      //     mainAxisSize: MainAxisSize.max,
      //     children: [
      //       Expanded(
      //         child: ListView(
      //           shrinkWrap: true,
      //           padding: EdgeInsets.zero,
      //           children: <Widget>[
      //             DrawerHeader(
      //               decoration: BoxDecoration(
      //                 color: Theme.of(context).primaryColor,
      //               ),
      //               child: BlocBuilder<LoginBloc, LoginState>(
      //                 builder: (BuildContext context, LoginState state) => Text(
      //                   state is LoggedIn ? state.user.firstName : "Non connecté",
      //                   style: TextStyle(
      //                     color: Theme.of(context).colorScheme.onPrimary,
      //                     fontSize: 24,
      //                   ),
      //                 ),
      //               ),
      //             ),
      //             ListTile(
      //               onTap: () {},
      //               leading: Icon(Icons.account_circle),
      //               title: Text(
      //                 'Profil',
      //                 style: TextStyle(color: Theme.of(context).textTheme.bodyText1.color),
      //               ),
      //             ),
      //             ListTile(
      //               onTap: () {
      //                 Navigator.push(context, MaterialPageRoute(builder: (context) => Settings()));
      //               },
      //               leading: Icon(Icons.settings),
      //               title: Text(
      //                 'Paramètres',
      //                 style: TextStyle(color: Theme.of(context).textTheme.bodyText1.color),
      //               ),
      //             ),
      //             // ListTile(
      //             //   leading: Icon(Icons.exit_to_app),
      //             //   onTap: () {
      //             //     BlocProvider.of<LoginBloc>(context).add(UserLogout());
      //             //   },
      //             //   title: Text(
      //             //     'Se déconnecter',
      //             //     style: TextStyle(color: Theme.of(context).textTheme.bodyText1.color),
      //             //   ),
      //             // ),
      //           ],
      //         ),
      //       ),

      //     ],
      //   ),
      // ),
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle.light,
        title: Text(AppLocalizations.of(context)!.calendrier),
        actions: [
          IconButton(
            icon: const Icon(Icons.settings),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const Settings()));
            },
          )
        ],
        backgroundColor: Theme.of(context).primaryColor,
        //   bottom: TabBar(
        //     indicatorColor: Theme
        //         .of(context)
        //         .colorScheme
        //         .onPrimary,
        //     tabs: [
        //       Tab(
        //         text: 'Evénement',
        //       ),
        //       Tab(
        //         text: 'Intranet',
        //       ),
        //       Tab(
        //         child: Text(
        //           'Emploi du temps',
        //           overflow: TextOverflow.fade,
        //         ),
        //       ),
        //     ],
        //   ),
        //   actions: [
        //     IconButton(
        //       icon: Icon(
        //         Icons.notifications,
        //         color: Colors.white70,
        //       ),
        //       onPressed: () {},
        //     )
        //   ],
      ),
      body:
          // TabBarView(
          //   children: [
          const EventsTab(),
      // Container(),
      // Container(),
      // ],
      // ),
    );
  }
}
