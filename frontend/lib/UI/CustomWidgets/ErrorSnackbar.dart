import 'package:flutter/material.dart';

class ErrorSnackbar {
  static void showSnackbar(
      {required BuildContext context,
      required String text,
      String? label,
      void Function()? onPressed}) {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      action: onPressed == null
          ? null
          : SnackBarAction(
              label: label ?? '',
              textColor: Theme.of(context).primaryColor,
              onPressed: onPressed,
            ),
      content: Row(
        children: [
          const Padding(
            padding: EdgeInsets.only(right: 10),
            child: Icon(
              Icons.wifi_off_rounded,
              color: Colors.amber,
            ),
          ),
          Expanded(
            child: Text(
              text,
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
        ],
      ),
      backgroundColor: Theme.of(context).cardColor,
    ));
  }
}
