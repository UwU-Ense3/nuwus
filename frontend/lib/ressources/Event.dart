import 'dart:typed_data';

import 'package:ense3/models/global.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';

class Event extends Equatable {
  const Event({
    required this.id,
    required this.name,
    required this.beginTime,
    required this.endTime,
    required this.location,
    required this.organizers,
    required this.description,
    required this.tags,
    this.numberViews,
    this.imageLink,
    this.imageBytes,
    this.createdBy,
    this.createdById,
    this.updatedBy,
    this.createdTime,
    this.updatedTime,
    this.price,
    this.numberParticipants,
  });

  final int id;
  final String name;
  final DateTime beginTime;
  final DateTime endTime;
  final String location;
  final List<Organizer> organizers;
  final String description;
  final List<Tag> tags;
  final int? numberParticipants;
  final int? price;
  final String? imageLink;
  final Uint8List? imageBytes;
  final int? numberViews;
  final String? createdBy;
  final int? createdById;
  final String? updatedBy;
  final DateTime? createdTime;
  final DateTime? updatedTime;

  factory Event.fromJson(Map<String, dynamic> json) {
    List<Organizer> organizers = [];
    for (Map<String, dynamic> orgJson in json['organizers']) {
      organizers.add(Organizer.fromJson(orgJson));
    }
    List<Tag> tags = [];
    for (Map<String, dynamic> tagJson in json['tags']) {
      tags.add(Tag.fromJson(tagJson));
    }
    return Event(
      id: json['id'],
      name: json['name'],
      beginTime: DateTime.parse(json['begin_time']),
      endTime: DateTime.parse(json['end_time']),
      location: json['location'],
      organizers: organizers,
      description: json['description'],
      tags: tags,
      numberParticipants: json['n_participants'],
      imageLink: json['image_link'],
      imageBytes: json['image_bytes'],
      numberViews: json['number_views'],
      price: json['price'],
      createdBy: json['created_by'],
      createdById: json['created_by_id'],
      updatedBy: json['updated_by'],
      createdTime: DateTime.tryParse(json['creation_time'] ?? ''),
      updatedTime: DateTime.tryParse(json['update_time'] ?? ''),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'begin_time': beginTime.toIso8601String(),
      'end_time': endTime.toIso8601String(),
      'location': location,
      'description': description,
      'n_participants': numberParticipants,
      'image_link': imageLink ?? '',
      'price': price,
      'tags': tags.map((tag) => tag.name).toList(),
      'organizers': organizers.map((org) => org.name).toList(),
    };
  }

  Event copyWith({
    int? id,
    String? name,
    DateTime? beginTime,
    DateTime? endTime,
    String? location,
    List<Organizer>? organizers,
    String? description,
    List<Tag>? tags,
    int? numberParticipants,
    int? price,
    String? imageLink,
    Uint8List? imageBytes,
    int? numberViews,
    String? createdBy,
    int? createdById,
    String? updatedBy,
    DateTime? createdTime,
    DateTime? updatedTime,
  }) {
    return Event(
      id: id ?? this.id,
      name: name ?? this.name,
      beginTime: beginTime ?? this.beginTime,
      endTime: endTime ?? this.endTime,
      location: location ?? this.location,
      organizers: organizers ?? this.organizers,
      description: description ?? this.description,
      tags: tags ?? this.tags,
      numberParticipants: numberParticipants ?? this.numberParticipants,
      imageLink: imageLink ?? this.imageLink,
      imageBytes: imageBytes ?? this.imageBytes,
      numberViews: numberViews ?? this.numberViews,
      price: price ?? this.price,
      createdBy: createdBy ?? this.createdBy,
      createdById: createdById ?? this.createdById,
      updatedBy: updatedBy ?? this.updatedBy,
      createdTime: createdTime ?? this.createdTime,
      updatedTime: updatedTime ?? this.updatedTime,
    );
  }

  String formatOrganizers() {
    var result = '';
    for (var i = 0; i < organizers.length; i++) {
      if (i > 0) {
        result += ', ';
      }
      result += organizers[i].name;
    }
    return result;
  }

  int durationInHours() {
    return endTime.difference(beginTime).inHours;
  }

  String formatFullDatetime(AppLocalizations? localisationProvider,
      {bool full = false}) {
    if (beginTime.day == endTime.day ||
        durationInHours() < 13 && durationInHours() >= 0) {
      return relativeDate(beginTime, localisationProvider, full: full) +
          DateFormat(localisationProvider!.de, localisationProvider.localeName)
              .format(beginTime) +
          DateFormat(localisationProvider.a, localisationProvider.localeName)
              .format(endTime);
    }
    if (beginTime.month == endTime.month) {
      return DateFormat(
          localisationProvider!.du, localisationProvider.localeName)
          .format(beginTime) +
          DateFormat(localisationProvider.au, localisationProvider.localeName)
              .format(endTime);
    } else {
      return DateFormat(localisationProvider!.du_mois,
          localisationProvider.localeName)
          .format(beginTime) +
          DateFormat(localisationProvider.au, localisationProvider.localeName)
              .format(endTime);
    }
  }

  @override
  String toString() {
    return '[$id] Event $name : \n'
        '   $beginTime - $endTime\n'
        '   location : $location\n'
        '   organizers : $organizers\n';
  }

  @override
  List<Object?> get props => [
    id,
    name,
    beginTime,
    endTime,
    location,
    organizers,
    description,
    tags,
    numberParticipants,
    imageLink,
    price,
    createdBy,
    updatedBy,
    createdTime,
    updatedTime,
  ];
}

class Organizer extends Equatable {
  final int id;
  final String name;
  static Map<int, Future<Organizer>> lookupDic = {};

  const Organizer(this.id, this.name);

  factory Organizer.fromJson(Map<String, dynamic> json) {
    return Organizer(json['id'], json['name']);
  }

  // static Future<Organizer>? organizerFromId(int id) {
  //   if (lookupDic.containsKey(id)) {
  //     return lookupDic[id];
  //   }
  //   var repository = OrganizerRepository();
  //   var org = repository.fetchOrganizer(id);
  //   lookupDic[id] = org;
  //   return org;
  // }

  @override
  String toString() {
    return 'Organizer $name';
  }

  @override
  List<Object> get props => [id, name];
}

class Tag extends Equatable {
  final int id;
  final String name;
  final String color;

  static Map<int, Future<Tag>> lookupDic = {};

  const Tag(this.id, this.name, this.color);

  factory Tag.fromJson(Map<String, dynamic> json) {
    return Tag(json['id'], json['name'], json['color']);
  }

  // static Future<Tag>? tagFromId(int id) {
  //   if (lookupDic.containsKey(id)) {
  //     return lookupDic[id];
  //   }
  //   var repository = TagRepository();
  //   var org = repository.fetchTag(id);
  //   lookupDic[id] = org;
  //   return org;
  // }

  @override
  String toString() {
    return 'Tag $name';
  }

  @override
  List<Object> get props => [id, name, color];
}
