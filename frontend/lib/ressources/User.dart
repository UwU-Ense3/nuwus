class User {
  int id;
  String firstName;
  String lastName;
  String mail;
  String authToken;
  bool canCreateEvent;
  bool canModifyAllEvents;

  User.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        firstName = json['first_name'],
        lastName = json['last_name'],
        mail = json['mail'],
        authToken = json['token'],
        canCreateEvent = json['can_create_event'],
        canModifyAllEvents = json['can_modify_all_events'];

  // Map<String, dynamic> toJson() => {
  //       'first_name': firstName,
  //       'last_name': lastName,
  //       'mail': mail,
  //     };

  @override
  String toString() {
    return 'User [$id] $firstName $lastName\n  email: $mail\n  token: $authToken';
  }
}
