# Models

## Global

Dans `global` on définit les comportements de scroll et les couleurs utilisées dans toute l'application. 

## Palette

`palette` est uniquement utilisé pour générer une palette de couleurs 
sur la base d'une couleur primaire donnée. 

[Source][https://medium.com/@morgenroth/using-flutters-primary-swatch-with-a-custom-materialcolor-c5e0f18b95b0 ]

## ThemeSaver

`ThemeSaver` gère l'enregistrement et le chargement du thème 
à chaque relance de l'application.

## AuthSaver

`AuthSaver` gère l'enregistrement et le chargement du de l'utilisateur connecté à chaque relance de l'application.