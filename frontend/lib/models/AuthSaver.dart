/// Ce fichier gère la sauvegarde des identifiants de connexions (mail + token de l'utilisateur) entre
/// les lancements de l'application. On utilise pour cela flutter_secure_storage pour les stocker de
/// manière sécurisée (chiffrée)
/// Sur le web, on utilise shared_preferences, bien moins sécurisé...
import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthSaver {
  static const String _keyToken = 'auth_key';
  static const String _keyMail = 'mail';
  static const _secureStorage = FlutterSecureStorage();

  static Future<List<String?>> loadUser() async {
    if (!kIsWeb) {
      String? userToken = await _secureStorage.read(key: _keyToken);
      String? mail = await _secureStorage.read(key: _keyMail);
      return [mail, userToken];
    }
    final prefs = await SharedPreferences.getInstance();
    String? userToken = prefs.getString(_keyToken);
    String? mail = prefs.getString(_keyMail);
    return [mail, userToken];
  }

  static Future<void> saveUser(String mail, String userToken) async {
    if (!kIsWeb) {
      _secureStorage.write(key: _keyToken, value: userToken);
      _secureStorage.write(key: _keyMail, value: mail);
    } else {
      final prefs = await SharedPreferences.getInstance();
      prefs.setString(_keyToken, userToken);
      prefs.setString(_keyMail, mail);
    }
  }
}
