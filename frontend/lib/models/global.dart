// import 'package:ense3/generated/l10n.dart';
import 'package:dart_date/dart_date.dart';
import 'package:ense3/models/palette.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:intl/intl.dart';

/// pour pouvoir utiliser des string dans Color() au lieu d'un int
extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "0xff".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('0xff', '').replaceFirst('0xFF', ''));
    return Color(int.parse(buffer.toString(), radix: 16)).withOpacity(1);
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) =>
      '${leadingHashSign ? 'oxff' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}

/// iOS style scroll behavior : no glow and overscroll
class BoxScrollBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

/// Scroll behavior with no glow on overscroll
class NoGlow extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

/// Affiche une snackbar avec une icone et un texte donnés
void snackBar(BuildContext context, IconData icon, String text) {
  ScaffoldMessenger.of(context).removeCurrentSnackBar();
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      elevation: 3,
      content: Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: Icon(
              icon,
              color: Colors.amber,
            ),
          ),
          Expanded(
            child: Text(
              text,
              style: Theme.of(context)
                  .textTheme
                  .bodyText1!
                  .copyWith(color: Theme.of(context).backgroundColor),
            ),
          ),
        ],
      ),
      backgroundColor: Theme.of(context).textTheme.bodyText2!.color,
    ),
  );
}

String relativeDate(DateTime dateTime, AppLocalizations? localizationProvider,
    {bool full = false}) {
  var today = DateTime.now().endOfDay;
  // avant aujourd'hui
  if (dateTime.isBefore(today.add(const Duration(days: -1)))) {
    return DateFormat('dd MMM yyyy', localizationProvider!.localeName)
        .format(dateTime);
  }
  // aujourd'hui
  if (dateTime.isBefore(today)) {
    return localizationProvider!.aujourdhui;
    // return "Aujourd'hui";
  }
  // demain
  if (dateTime.isBefore(today.add(const Duration(days: 1)))) {
    return localizationProvider!.demain;
    // return "Demain";
  }
  if (dateTime.isBefore(today.add(const Duration(days: 6)))) {
    var text =
        DateFormat('EEEE', localizationProvider!.localeName).format(dateTime);
    return '${text[0].toUpperCase()}${text.substring(1)}';
  }
  if (dateTime.isBefore(today.endOfMonth)) {
    var text = DateFormat('EEEE dd', localizationProvider!.localeName)
        .format(dateTime);
    return '${text[0].toUpperCase()}${text.substring(1)}';
  } else {
    if (full) {
      var text = DateFormat('EEEE dd MMMM', localizationProvider!.localeName)
          .format(dateTime);
      return '${text[0].toUpperCase()}${text.substring(1)}';
    }
    return DateFormat('dd MMM', localizationProvider!.localeName)
        .format(dateTime);
  }
}

/// Colors of dark and light theme
class ThemeColors {
  // light theme colors
  static const Color _e3Blue = Color(0xFF0054A6);
  static final MaterialColor _lightPrimary = generateMaterialColor(_e3Blue);
  static const Color _lightBackground = Color(0xffffffff);
  static const Color _lightAccent = Color(0xFF69A9CA);
  static const Color _lightBorder = Color(0xFFD1D1D1);
  static const Color _lightContainer = Color(0xfff8f8f8);
  static const Color _lightText = Color(0xFF343434);

  // dark theme colors
  static const Color _uwuGreen = Color(0xff2AB528);
  static final MaterialColor _darkPrimary = generateMaterialColor(_uwuGreen);
  static const Color _darkAccent = Color(0xff2E6A2C);
  static const Color _darkBackground = Color(0xFF1d1d1d);
  static const Color _darkBorder = Color(0xFF37E833);
  static const Color _darkContainer = Color(0xff000000);
  static const Color _darkText = Color(0xFFD1D1D1);
  static const Color _darkChip = Color(0xff2E6A2C);

  // common colors
  static const Color _onPrimary = Colors.white;
  static const Color barColor = Color(0xFF7D498F);
  static const Color autresColor = Color(0xFF388738);
  static const Color repasColor = Color(0xFF3F6591);
  static const Color conferenceColor = Color(0xFFB19D3D);
  static const Color soireeColor = Color(0xFF9A3E3E);

  static final ThemeData lightTheme = ThemeData.light().copyWith(
    brightness: Brightness.light,
    primaryColor: _lightPrimary,
    backgroundColor: _lightBackground,
    dividerColor: _lightBorder,
    cardColor: _lightContainer,
    colorScheme: ColorScheme.light(
      primary: _lightPrimary,
      onPrimary: _onPrimary,
      secondary: _lightAccent,
      background: _lightBackground,
    ),
    appBarTheme: AppBarTheme(color: _lightPrimary),
    textTheme: TextTheme(
      bodyText1: const TextStyle(
        color: _lightText,
        fontSize: 14,
        fontFamily: 'Roboto',
        fontWeight: FontWeight.normal,
      ),
      headline2: const TextStyle(
        fontFamily: 'Roboto',
        fontWeight: FontWeight.w600,
        color: _lightText,
        fontSize: 16,
      ),
      bodyText2: const TextStyle(
        fontFamily: 'Roboto',
        color: _lightText,
        fontSize: 20,
      ),
      headline1: TextStyle(
        fontFamily: 'Roboto',
        fontWeight: FontWeight.w300,
        color: _lightPrimary,
        fontSize: 16,
      ),
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: OutlinedButton.styleFrom(
        primary: _lightPrimary,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(4))),
        side: BorderSide(
          color: _lightPrimary.withOpacity(0.12),
          width: 1,
        ),
        textStyle: TextStyle(
          fontFamily: 'Roboto',
          color: _lightPrimary,
        ),
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        primary: _lightPrimary,
        textStyle: const TextStyle(
          fontFamily: 'Roboto',
          color: _onPrimary,
        ),
      ),
    ),
    inputDecorationTheme: InputDecorationTheme(
      labelStyle: TextStyle(
        color: _lightText.withOpacity(0.5),
        fontSize: 14,
        fontFamily: 'Roboto',
        fontWeight: FontWeight.normal,
      ),
      filled: true,
      fillColor: const Color(0x05000000),
      enabledBorder: const OutlineInputBorder(
          borderSide: BorderSide(
            color: Color(0x812D2D2D),
            width: 1.8,
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
          gapPadding: 4),
      focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: _lightPrimary,
            width: 2,
            style: BorderStyle.solid,
          ),
          borderRadius: const BorderRadius.all(Radius.circular(5.0)),
          gapPadding: 4),
    ),
  );

  static final ThemeData darkTheme = ThemeData.dark().copyWith(
    brightness: Brightness.dark,
    primaryColor: _darkPrimary,
    backgroundColor: _darkBackground,
    scaffoldBackgroundColor: _darkBackground,
    dialogBackgroundColor: _darkBackground,
    canvasColor: _darkBackground,
    dividerColor: _darkBorder,
    cardColor: _darkContainer,
    chipTheme: ChipThemeData.fromDefaults(
      secondaryColor: _darkChip,
      brightness: Brightness.dark,
      labelStyle: const TextStyle(
        color: _darkText,
      ),
    ).copyWith(selectedColor: _darkChip),
    colorScheme: ColorScheme.dark(
      primary: _darkPrimary,
      onPrimary: _onPrimary,
      secondary: _darkAccent,
      background: _darkBackground,
    ),
    appBarTheme: AppBarTheme(color: _darkPrimary),
    textTheme: TextTheme(
      bodyText1: const TextStyle(
        color: _darkText,
        fontSize: 14,
        fontFamily: 'Roboto',
        fontWeight: FontWeight.normal,
      ),
      headline2: const TextStyle(
        fontFamily: 'Roboto',
        fontWeight: FontWeight.w600,
        color: _darkText,
        fontSize: 16,
      ),
      bodyText2: const TextStyle(
        fontFamily: 'Roboto',
        color: _darkText,
        fontSize: 20,
      ),
      headline1: TextStyle(
        fontFamily: 'Roboto',
        fontWeight: FontWeight.w100,
        color: _darkPrimary,
        fontSize: 14,
      ),
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: OutlinedButton.styleFrom(
        primary: _darkPrimary,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(4))),
        side: BorderSide(
          color: _darkPrimary.withOpacity(0.12),
          width: 1,
        ),
        textStyle: TextStyle(
          color: _darkPrimary,
        ),
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        primary: _darkPrimary,
        textStyle: const TextStyle(
          fontFamily: 'Roboto',
          color: _onPrimary,
        ),
      ),
    ),
    inputDecorationTheme: InputDecorationTheme(
      labelStyle: TextStyle(
        color: _darkText.withOpacity(0.5),
        fontSize: 14,
        fontFamily: 'Roboto',
        fontWeight: FontWeight.normal,
      ),
      filled: true,
      fillColor: const Color(0x05000000),
      enabledBorder: const OutlineInputBorder(
          borderSide: BorderSide(
            color: Color(0x812D2D2D),
            width: 1.8,
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
          gapPadding: 4),
      focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: _darkPrimary,
            width: 2,
            style: BorderStyle.solid,
          ),
          borderRadius: const BorderRadius.all(Radius.circular(5.0)),
          gapPadding: 4),
    ),
  );
}

/// Markdown formatting :
MarkdownStyleSheet getMarkdownStyleSheet(BuildContext context) {
  return MarkdownStyleSheet(
    h1: Theme.of(context)
        .textTheme
        .bodyText2!
        .copyWith(fontWeight: FontWeight.bold),
    h2: Theme.of(context)
        .textTheme
        .bodyText2!
        .copyWith(fontSize: 18, fontWeight: FontWeight.w500),
    code: Theme.of(context)
        .textTheme
        .bodyText2!
        .copyWith(fontSize: 15, backgroundColor: Colors.grey.withOpacity(0.2)),
    blockquote: Theme.of(context).textTheme.bodyText2,
    blockquoteDecoration: BoxDecoration(
      color: Theme.of(context).primaryColor.withOpacity(0.2),
      borderRadius: BorderRadius.circular(2.0),
    ),
    p: Theme.of(context).textTheme.bodyText1,
    a: Theme.of(context)
        .textTheme
        .bodyText1!
        .copyWith(color: Colors.blueAccent),
  );
}
