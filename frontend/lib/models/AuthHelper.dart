import 'dart:convert';

import 'package:ense3/networking/ApiHelper.dart';
import 'package:ense3/ressources/User.dart';
import 'package:universal_io/io.dart';

class AuthHelper {
  AuthHelper({required String baseUrl}) : _api = ApiHelper(baseUrl: baseUrl);

  final ApiHelper _api;

  Future<User> login(String mail, String userToken) async {
    Map<String, String> headers = {'mail': mail, 'password': userToken};
    var json = await _api.post('api/login', '', headers: headers);
    User user = User.fromJson(json);
    user.authToken = userToken;
    return user;
  }

  Future<void> startLogin(String mail) async {
    await _api.post('api/login_start/$mail', '');
  }

  Future<User> passwordlessLogin(code) async {
    var respJson = await _api.post('api/login/$code', '');
    User user = User.fromJson(respJson);
    return user;
  }

  Future<void> register(String mail, String firstName, String lastName) async {
    var body = {
      'mail': mail,
      'first_name': firstName,
      'last_name': lastName,
    };
    await _api.post('api/register', json.encode(body));
  }

  Future<bool> checkEventCreatePermission(String mail, String password) async {
    try {
      HttpResponse response =
          await _api.get('api/user/create_event_permission', headers: {
        'mail': mail,
        'password': password,
      });
      return response.statusCode == 200;
    } on HttpResponseException catch (e) {
      if (e.statusCode == 401) {
        return false;
      }
      rethrow;
    }
  }
}
