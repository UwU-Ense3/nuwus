import 'dart:async';

import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:theme_mode_handler/theme_mode_manager_interface.dart';

class MyManager implements IThemeModeManager {
  static const String _key = 'theme';

  @override
  Future<String?> loadThemeMode() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Logger().d('Loaded theme : ${prefs.get(_key)}');
    return prefs.get(_key) as FutureOr<String?>;
  }

  @override
  Future<bool> saveThemeMode(String value) async {
    final prefs = await SharedPreferences.getInstance();
    Logger().d('Writing theme : $value');
    return prefs.setString(_key, value);
  }
}
