import 'dart:async';

import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalizationSaver {
  static const String _key = 'locale';

  Future<String?> loadLocale() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Logger().d('Loaded locale : ${prefs.get(_key)}');
    return prefs.get(_key) as FutureOr<String?>;
  }

  Future<bool> saveLocale(String value) async {
    final prefs = await SharedPreferences.getInstance();
    Logger().d('Writing locale : $value');
    return prefs.setString(_key, value);
  }
}
