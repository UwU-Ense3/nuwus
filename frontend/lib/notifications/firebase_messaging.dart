import 'package:ense3/UI/EventsTab/event_view_page.dart';
import 'package:ense3/bloc/events_list_bloc/event_list_bloc.dart';
import 'package:ense3/firebase_options.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:logger/logger.dart';

void registerNotification() async {
  // don't subscribe to notifications if running on the web
  if (kIsWeb) {
    return;
  }

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  var messaging = FirebaseMessaging.instance;

  messaging.subscribeToTopic('all');

  // On iOS, this helps to take the user permissions
  await messaging.requestPermission(
    alert: true,
    badge: true,
    provisional: false,
    sound: true,
  );
  Logger()
      .d('Firebase device id : ${await FirebaseMessaging.instance.getToken()}');
}

void handleMessage(RemoteMessage message, BuildContext context) {
  Navigator.of(context).push(
    MaterialPageRoute(
      builder: (context) => BlocBuilder<EventsListBloc, EventsListState>(
        builder: (context, state) {
          if (state is EventsListLoadInProgress) {
            return const Center(child: CircularProgressIndicator());
          }
          if (state is EventsListLoadSuccess) {
            return EventViewPage(
              event: state.eventsList.firstWhere(
                  (element) => element.id == message.data['event_id']),
            );
          }
          Logger().e(state.toString());
          return const Center(child: Text('Error'));
        },
      ),
    ),
  );
}
