import 'package:bloc/bloc.dart';
import 'package:ense3/firebase_options.dart';
import 'package:ense3/networking/ApiHelper.dart';
import 'package:ense3/repository/EventRepository.dart';
import 'package:ense3/ressources/Event.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:logger/logger.dart';

part 'firebase_notif_state.dart';

class FirebaseNotifCubit extends Cubit<FirebaseNotifState> {
  FirebaseNotifCubit({required String apiUrl})
      : eventRepository = EventRepository(apiUrl: apiUrl),
        super(FirebaseNotifInitial()) {
    initializeFirebase();
  }

  final eventRepository;

  void initializeFirebase() async {
    // don't subscribe to notifications if running on the web
    if (kIsWeb) {
      return;
    }

    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );

    var messaging = FirebaseMessaging.instance;

    messaging.subscribeToTopic('all');

    // On iOS, this helps to take the user permissions
    await messaging.requestPermission(
      alert: true,
      badge: true,
      provisional: false,
      sound: true,
    );
    Logger().d('Firebase device id : ${await messaging.getToken()}');

    // Get any messages which caused the application to open from
    // a terminated state.
    RemoteMessage? initialMessage =
        await FirebaseMessaging.instance.getInitialMessage();

    // If the message also contains a data property navigate to the event page
    if (initialMessage != null) {
      _handleMessage(initialMessage);
    }

    // Also handle any interaction when the app is in the background via a
    // Stream listener
    FirebaseMessaging.onMessageOpenedApp.listen(_handleMessage);
  }

  void _handleMessage(RemoteMessage message) async {
    emit(FirebaseNotifLoading());
    try {
      if (message.data.keys.contains('event_id') == false) {
        emit(FirebaseNotifNotFoundError());
        return;
      }
      final event =
          await eventRepository.fetchEvent(int.parse(message.data['event_id']));
      emit(FirebaseNotifDataAvailable(event: event));
    } on HttpResponseException catch (e) {
      Logger().e(e);
      if (e.statusCode == 404) {
        emit(FirebaseNotifNotFoundError());
        return;
      }
      emit(FirebaseNotifNetworkError());
    }
  }

  void reset() {
    emit(FirebaseNotifInitial());
  }

  @override
  void onChange(Change<FirebaseNotifState> change) {
    Logger().d(change);
    super.onChange(change);
  }
}
