part of 'firebase_notif_cubit.dart';

@immutable
abstract class FirebaseNotifState {}

class FirebaseNotifInitial extends FirebaseNotifState {}

class FirebaseNotifLoading extends FirebaseNotifState {}

class FirebaseNotifDataAvailable extends FirebaseNotifState {
  final Event event;

  FirebaseNotifDataAvailable({required this.event});
}

class FirebaseNotifNetworkError extends FirebaseNotifState {}

class FirebaseNotifNotFoundError extends FirebaseNotifState {}
