import 'dart:convert';

import 'package:ense3/networking/ApiHelper.dart';
import 'package:ense3/ressources/Event.dart';

class EventRepository {
  EventRepository({required String apiUrl}) : _api = ApiHelper(baseUrl: apiUrl);

  final String url = 'api/event';
  final ApiHelper _api;

  Future<List<Event>> fetchEventsList({hidePast = true}) async {
    final response = await _api.get(url, headers: {
      'hide_past': hidePast.toString(),
    });
    var eventsList = <Event>[];
    response.forEach((v) {
      eventsList.add(Event.fromJson(v));
    });
    return eventsList;
  }

  Future<Event> fetchEvent(int id) async {
    final response = await _api.get('$url/$id');
    return Event.fromJson(response);
  }

  Future<Event> createEvent(Event event,
      String mail,
      String password, {
        required bool sendNotification,
        required String notificationText,
      }) async {
    final body = event.toJson()
      ..addAll({
        'send_notification': sendNotification,
        'notification_text': notificationText,
      });
    final response = await _api.post(url, json.encode(body), headers: {
      'mail': mail,
      'password': password,
    });
    return Event.fromJson(response);
  }

  Future<Event> editEvent(int id,
      Event event,
      String mail,
      String password, {
        required bool sendNotification,
        required String notificationText,
      }) async {
    final body = event.toJson()
      ..addAll({
        'send_notification': sendNotification,
        'notification_text': notificationText,
      });

    final response = await _api.put('$url/$id', json.encode(body), headers: {
      'mail': mail,
      'password': password,
    });
    return Event.fromJson(response);
  }

  Future<void> deleteEvent(int id, String mail, String password) async {
    await _api.delete('$url/$id', headers: {
      'mail': mail,
      'password': password,
    });
  }
}

class OrganizerRepository {
  OrganizerRepository({required String apiUrl})
      : _api = ApiHelper(baseUrl: apiUrl);
  final String url = 'api/organizer';
  final ApiHelper _api;

  Future<List<Organizer>> fetchOrganizersList() async {
    final response = await _api.get(url);
    var organizersList = <Organizer>[];
    response.forEach((v) {
      organizersList.add(Organizer.fromJson(v));
    });
    return organizersList;
  }

  Future<Organizer> fetchOrganizer(int id) async {
    final response = await _api.get('$url/$id');
    return Organizer.fromJson(response);
  }
}

class TagRepository {
  TagRepository({required String apiUrl}) : _api = ApiHelper(baseUrl: apiUrl);
  final String url = 'api/tag';
  final ApiHelper _api;

  Future<List<Tag>> fetchTagsList() async {
    final response = await _api.get(url);
    var tagsList = <Tag>[];
    response.forEach((v) {
      tagsList.add(Tag.fromJson(v));
    });
    return tagsList;
  }

  Future<Tag> fetchTag(int id) async {
    final response = await _api.get('$url/$id');
    return Tag.fromJson(response);
  }
}
