import 'dart:convert';

import 'package:ense3/networking/ApiHelper.dart';
import 'package:ense3/ressources/User.dart';

class UserRepository {
  UserRepository({required String baseUrl})
      : _api = ApiHelper(baseUrl: baseUrl);

  final String url = 'api/user';
  final ApiHelper _api;

  Future<List<User>> fetchUsersList() async {
    final response = await _api.get(url);
    var usersList = <User>[];
    response.forEach((v) {
      usersList.add(User.fromJson(v));
    });
    return usersList;
  }

  Future<User> fetchUser(String key) async {
    final response = await _api.get('$url/$key');
    return User.fromJson(response);
  }

  Future<String> register(User user) async {
    final response = await _api.post(url, json.encode(user));
    return response['keys'][0];
  }
}
