import 'dart:ui';

import 'package:ense3/bloc/events_list_bloc/event_list_bloc.dart';
import 'package:ense3/bloc/localization_cubit/localization_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:logger/logger.dart';
import 'package:theme_mode_handler/theme_mode_handler.dart';
import 'package:universal_io/io.dart';

import 'UI/CustomWidgets/ErrorSnackbar.dart';
import 'UI/Home.dart';
import 'app_config.dart';
import 'authentication/login_bloc/LoginBloc.dart';
import 'models/ThemeSaver.dart';
import 'models/global.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  // firebase notifs disabled on debug
  // registerNotification();

  runApp(
    AppConfig(
      flavorName: 'dev',
      apiBaseUrl: 'http://localhost:5000/',
      child: MultiBlocProvider(
        providers: [
          //Bloc provider pour stocker la langue de l'application
          BlocProvider(create: (BuildContext context) {
            var localizationCubit = LocalizationCubit(
                Locale(Platform.localeName.contains('fr') ? 'fr' : 'en'));
            Logger()
                .d('Device language: ${localizationCubit.state.toString()}');
            localizationCubit.loadSavedLocale();
            return localizationCubit;
          }),
          // Bloc provider pour stocker l'état d'authentification
          BlocProvider(
              create: (BuildContext context) => LoginBloc(
                  apiUrl: context
                      .findAncestorWidgetOfExactType<AppConfig>()!
                      .apiBaseUrl)
                ..add(AppLoaded())),
          // Bloc provider pour stocker l'état de la liste d'événements
          BlocProvider(
              create: (BuildContext context) => EventsListBloc(
                  apiUrl: context
                      .findAncestorWidgetOfExactType<AppConfig>()!
                      .apiBaseUrl)),
        ],
        child: const MyApp(),
      ),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ThemeModeHandler(
        // Permet de gérer le thème, de le changer et de stocker l'état actuel du theme (dark ou light)
        manager: MyManager(),
        defaultTheme: ThemeMode.light,
        builder: (ThemeMode themeMode) {
          return BlocBuilder<LocalizationCubit, Locale>(
            builder: (context, locale) {
              return MaterialApp(
                debugShowCheckedModeBanner: false,
                localizationsDelegates: const [
                  AppLocalizations.delegate,
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                  GlobalCupertinoLocalizations.delegate,
                ],
                locale: locale,
                supportedLocales: AppLocalizations.supportedLocales,
                scrollBehavior: const MaterialScrollBehavior().copyWith(
                  dragDevices: {
                    PointerDeviceKind.mouse,
                    PointerDeviceKind.touch
                  },
                ),
                title: 'NUwUs',
                themeMode: themeMode,
                darkTheme: ThemeColors.darkTheme,
                theme: ThemeColors.lightTheme,
                home: BlocListener<LoginBloc, LoginState>(
                  listener: (context, state) {
                    if (state is ConnectionError) {
                      ErrorSnackbar.showSnackbar(
                        context: context,
                        text: 'Erreur de connexion au serveur',
                      );
                    }
                  },
                  child: Home(),
                ),
              );
            },
          );
        });
  }
}
