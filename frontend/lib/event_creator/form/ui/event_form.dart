import 'package:ense3/app_config.dart';
import 'package:ense3/authentication/login_bloc/LoginBloc.dart';
import 'package:ense3/event_creator/form/cubit/event_form_cubit.dart';
import 'package:ense3/models/global.dart';
import 'package:ense3/ressources/Event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'event_form_body.dart';

class EventForm extends StatelessWidget {
  const EventForm({Key? key, this.event, this.editMode = false})
      : super(key: key);
  final Event? event;
  final bool editMode;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<EventFormCubit>(
      create: (context) => EventFormCubit(
        context.read<LoginBloc>(),
        sendNotification: editMode ? false : true,
        event: event,
        isLocalImage: false,
        apiUrl: context.findAncestorWidgetOfExactType<AppConfig>()!.apiBaseUrl,
      ),
      child: Builder(
        builder: (BuildContext context) => Scaffold(
          appBar: AppBar(
            title: Text(editMode
                ? AppLocalizations.of(context)!.editAnEvent
                : AppLocalizations.of(context)!.createAnEvent),
            actions: [
              BlocBuilder<EventFormCubit, EventFormState>(
                builder: (context, state) {
                  return IconButton(
                    icon: Icon(
                      Icons.delete_rounded,
                      size: 24,
                      color: Theme.of(context).colorScheme.onPrimary,
                    ),
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (_) {
                          return BlocListener<EventFormCubit, EventFormState>(
                            bloc: context.read<EventFormCubit>(),
                            listener: (context, state) {
                              if (state is EventFormRequestSuccess) {
                                Navigator.of(context)
                                  ..pop()
                                  ..pop();
                                snackBar(
                                    context,
                                    Icons.auto_awesome_rounded,
                                    AppLocalizations.of(context)!
                                        .eventDeleted(state.event.name));
                              } else if (state is EventFormRequestFailed) {
                                Navigator.of(context).pop();
                                snackBar(context, Icons.wifi_off_rounded,
                                    state.message);
                              }
                            },
                            child: AlertDialog(
                              title: Text(
                                AppLocalizations.of(context)!.deleteEvent,
                                style: Theme.of(context).textTheme.bodyText2,
                              ),
                              content: Text(
                                AppLocalizations.of(context)!
                                    .reallyDeleteEvent(state.event.name),
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                              actions: [
                                OutlinedButton(
                                    onPressed: () =>
                                        Navigator.of(context).pop(),
                                    child: Text(
                                        AppLocalizations.of(context)!.cancel)),
                                ElevatedButton(
                                  child: Text(
                                      AppLocalizations.of(context)!.delete),
                                  onPressed: () {
                                    context
                                        .read<EventFormCubit>()
                                        .deleteEvent(state.event.id);
                                  },
                                ),
                              ],
                            ),
                          );
                        },
                      );
                    },
                  );
                },
              ),
            ],
          ),
          body: EventFormBody(editMode: editMode),
        ),
      ),
    );
  }
}
