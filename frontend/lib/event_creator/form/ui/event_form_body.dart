import 'package:dart_date/dart_date.dart';
import 'package:ense3/bloc/events_list_bloc/event_list_bloc.dart';
import 'package:ense3/event_creator/form/cubit/event_form_cubit.dart';
import 'package:ense3/event_creator/form/ui/event_verif_page.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'date_time_picker.dart';

class EventFormBody extends StatefulWidget {
  EventFormBody({Key? key, required this.editMode}) : super(key: key);
  bool editMode;

  @override
  State<EventFormBody> createState() => _EventFormBodyState();
}

class _EventFormBodyState extends State<EventFormBody> {
  final _nameController = TextEditingController();
  final _descriptionController = TextEditingController();
  final _locationController = TextEditingController();
  final _tagsController = TextEditingController();
  final _priceController = TextEditingController();
  final _beginTimeController = TextEditingController();
  final _endTimeController = TextEditingController();
  final _organizersController = TextEditingController();
  final _maxParticipantsController = TextEditingController();
  final _notificationTextController = TextEditingController();

  void updateFromCubit(BuildContext context) {
    var state = BlocProvider.of<EventFormCubit>(context).state;
    final event = state.event;
    _nameController.text = event.name;
    _beginTimeController.text = event.beginTime.toString();
    _endTimeController.text = event.endTime.toString();
    _locationController.text = event.location;
    _maxParticipantsController.text = event.numberParticipants.toString();
    _priceController.text = ((event.price ?? 0) / 100).toString();
    _descriptionController.text = event.description;
    _tagsController.text = event.tags.map((e) => e.name).join(', ');
    _organizersController.text = event.organizers.map((e) => e.name).join(', ');
    _notificationTextController.text = state.notificationText;
  }

  @override
  void initState() {
    super.initState();
    updateFromCubit(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<EventFormCubit, EventFormState>(
      listener: (context, state) {
        if (state is EventFormRequestSuccess) {
          context.read<EventFormCubit>().resetToDefault();
          updateFromCubit(context);
          // Navigator.pop(context);
          context.read<EventsListBloc>().add(ListUpdateRequested());
        }
      },
      child: ListView(
        children: [
          const _ImageField(),
          _NameField(controller: _nameController),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
            child: Text(AppLocalizations.of(context)!.schedules),
          ),
          _BeginDateTimeField(
              controller: _beginTimeController,
              endTimeController: _endTimeController),
          _EndDateTimeField(controller: _endTimeController),
          RetractableHelpMessage(
            title: AppLocalizations.of(context)!.eventOrganizers,
            message: AppLocalizations.of(context)!.eventOrganizersDescription,
          ),
          _OrganizersField(controller: _organizersController),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
            child: Text(AppLocalizations.of(context)!.locationTitle),
          ),
          _LocationField(controller: _locationController),
          RetractableHelpMessage(
              title: AppLocalizations.of(context)!.descriptionTitle,
              message: AppLocalizations.of(context)!.descriptionMessage),
          _DescriptionField(controller: _descriptionController),
          Row(
            children: [
              Expanded(child: _PriceField(controller: _priceController)),
              Expanded(
                  child: _MaxParticipantsField(
                      controller: _maxParticipantsController)),
            ],
          ),
          RetractableHelpMessage(
            title: AppLocalizations.of(context)!.tagsTitle,
            message: AppLocalizations.of(context)!.tagsMessage,
          ),
          _TagsField(controller: _tagsController),
          RetractableHelpMessage(
            title: AppLocalizations.of(context)!.notificationTitle,
            message: AppLocalizations.of(context)!.notificationMessage,
          ),
          _SendNotificationSwitch(),
          _NotificationTextField(controller: _notificationTextController),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
            child: BlocBuilder<EventFormCubit, EventFormState>(
                builder: (context, state) {
              return ElevatedButton(
                onPressed: () {
                  final cubit = context.read<EventFormCubit>();
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => BlocProvider.value(
                        value: cubit,
                        child: EventVerifPage(
                            event: state.event, editMode: widget.editMode),
                      ),
                    ),
                  );
                },
                child: Text(AppLocalizations.of(context)!.verifyEvent),
              );
            }),
          ),
        ],
      ),
    );
  }
}

class RetractableHelpMessage extends StatefulWidget {
  final String title;
  final String message;
  bool expanded = false;

  RetractableHelpMessage({Key? key, required this.title, required this.message})
      : super(key: key);

  @override
  State<RetractableHelpMessage> createState() => _RetractableHelpMessageState();
}

class _RetractableHelpMessageState extends State<RetractableHelpMessage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
          child: Row(
            children: [
              Expanded(child: Text(widget.title)),
              IconButton(
                icon: const Icon(Icons.info_outline_rounded),
                onPressed: () {
                  setState(() {
                    widget.expanded = !widget.expanded;
                  });
                },
                color: Theme.of(context).textTheme.bodyText2!.color,
              ),
            ],
          ),
        ),
        widget.expanded
            ? Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                child: Text(
                  widget.message,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              )
            : Container(),
      ],
    );
  }
}

class _ImageField extends StatefulWidget {
  const _ImageField({Key? key}) : super(key: key);

  @override
  State<_ImageField> createState() => _ImageFieldState();
}

class _ImageFieldState extends State<_ImageField> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        context.read<EventFormCubit>().setImage(FilePicker.platform.pickFiles(
          type: FileType.image,
              allowMultiple: false,
              withData: true,
            ));
      },
      child: BlocBuilder<EventFormCubit, EventFormState>(
        builder: (context, state) {
          return Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Container(
              height: 250,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                borderRadius:
                    const BorderRadius.vertical(bottom: Radius.circular(4)),
                boxShadow: [
                  BoxShadow(
                    offset: const Offset(1, 1),
                    blurRadius: 5,
                    color: Colors.black.withOpacity(0.25),
                  ),
                ],
                color: Theme.of(context).colorScheme.secondary,
              ),
              child: Stack(
                alignment: Alignment.center,
                fit: StackFit.expand,
                children: [
                  ClipRRect(
                    borderRadius:
                        const BorderRadius.vertical(bottom: Radius.circular(4)),
                    child: (state is EventFormEditing && state.isLocalImage)
                        ? Image.memory(
                            state.event.imageBytes ?? Uint8List(0),
                            fit: BoxFit.cover,
                            errorBuilder: (context, object, stackTrace) => Icon(
                              Icons.image_not_supported_rounded,
                              color: Theme.of(context)
                                  .colorScheme
                                  .onPrimary
                                  .withOpacity(0.2),
                              size: 150,
                            ),
                          )
                        : Image.network(
                            // todo : vérifier que l'on a pas un chemin local lors de l'ajout d'un événement
                      state.event.imageLink ?? '',
                            fit: BoxFit.cover,
                            errorBuilder: (context, object, stackTrace) => Icon(
                              Icons.image_not_supported_rounded,
                              color: Theme.of(context)
                                  .colorScheme
                                  .onPrimary
                                  .withOpacity(0.2),
                              size: 150,
                            ),
                          ),
                  ),
                  Center(
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Theme.of(context).colorScheme.onPrimary,
                        ),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(4)),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          AppLocalizations.of(context)!.chooseAnImage,
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(
                                  color:
                                      Theme.of(context).colorScheme.onPrimary),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

class _NameField extends StatelessWidget {
  const _NameField({Key? key, required this.controller}) : super(key: key);

  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: TextField(
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: AppLocalizations.of(context)!.nameEvent,
        ),
        controller: controller,
        onChanged: (value) {
          context.read<EventFormCubit>().setName(value);
        },
      ),
    );
  }
}

class _BeginDateTimeField extends StatelessWidget {
  const _BeginDateTimeField(
      {Key? key, required this.controller, required this.endTimeController})
      : super(key: key);
  final TextEditingController controller;
  final TextEditingController endTimeController;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: BlocBuilder<EventFormCubit, EventFormState>(
        builder: (context, state) {
          return DateTimePicker(
            style: Theme.of(context).textTheme.bodyLarge,
            controller: controller,
            use24HourFormat: true,
            locale: Locale(AppLocalizations.of(context)!.localeName),
            calendarTitle: AppLocalizations.of(context)!.beginDate,
            decoration: InputDecoration(
              icon: const Icon(Icons.event),
              label: Text(
                AppLocalizations.of(context)!.beginDateTime,
                style: Theme.of(context).textTheme.bodyLarge,
              ),
            ),
            firstDate: DateTime(2000),
            lastDate: DateTime(2100),
            type: DateTimePickerType.dateTime,
            onChanged: (value) {
              var newTime = DateTime.parse(value);
              if (state.event.endTime < newTime) {
                context.read<EventFormCubit>().setEndDateTime(
                    newTime.add(const Duration(hours: 2)).toString());
                endTimeController.text =
                    newTime.add(const Duration(hours: 2)).toString();
              }
              context.read<EventFormCubit>().setBeginDateTime(value);
            },
          );
        },
      ),
    );
  }
}

class _EndDateTimeField extends StatelessWidget {
  const _EndDateTimeField({Key? key, required this.controller})
      : super(key: key);
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: BlocBuilder<EventFormCubit, EventFormState>(
        builder: (context, state) {
          return DateTimePicker(
            controller: controller,
            style: Theme.of(context).textTheme.bodyText1,
            use24HourFormat: true,
            locale: Locale(AppLocalizations.of(context)!.localeName),
            calendarTitle: AppLocalizations.of(context)!.endTime,
            decoration: InputDecoration(
              icon: const Icon(Icons.event),
              label: Text(
                AppLocalizations.of(context)!.endDateTime,
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ),
            firstDate: DateTime(2000),
            lastDate: DateTime(2100),
            type: DateTimePickerType.dateTime,
            onChanged: (value) {
              context.read<EventFormCubit>().setEndDateTime(value);
            },
            selectableDayPredicate: (endDateTime) {
              return endDateTime.day >= state.event.beginTime.day;
            },
            autovalidate: true,
            validator: (value) {
              if (DateTime.parse(value ?? '')
                  .isSameOrBefore(state.event.beginTime)) {
                return AppLocalizations.of(context)!.verifySchedules;
              } else {
                return null;
              }
            },
          );
        },
      ),
    );
  }
}

class _LocationField extends StatelessWidget {
  const _LocationField({Key? key, required this.controller}) : super(key: key);
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: TextField(
        style: Theme.of(context).textTheme.bodyText1,
        decoration: InputDecoration(
          labelText: AppLocalizations.of(context)!.locationEvent,
        ),
        controller: controller,
        onChanged: (value) {
          context.read<EventFormCubit>().setLocation(value);
        },
      ),
    );
  }
}

class _OrganizersField extends StatelessWidget {
  const _OrganizersField({Key? key, required this.controller})
      : super(key: key);
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: TextField(
        decoration: InputDecoration(
          labelText: AppLocalizations.of(context)!.organizersSeparated,
        ),
        style: Theme.of(context).textTheme.bodyText1,
        controller: controller,
        onChanged: (value) {
          context.read<EventFormCubit>().setOrganizers(value);
        },
      ),
    );
  }
}

class _DescriptionField extends StatelessWidget {
  const _DescriptionField({Key? key, required this.controller})
      : super(key: key);

  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: TextField(
        minLines: 4,
        maxLines: null,
        decoration: InputDecoration(
          labelText: AppLocalizations.of(context)!.description,
          alignLabelWithHint: true,
        ),
        style: Theme.of(context).textTheme.bodyText1,
        controller: controller,
        onChanged: (value) {
          context.read<EventFormCubit>().setDescription(value);
        },
      ),
    );
  }
}

class _TagsField extends StatelessWidget {
  const _TagsField({Key? key, required this.controller}) : super(key: key);
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: TextField(
        decoration: InputDecoration(
          labelText: AppLocalizations.of(context)!.tagsSeparated,
        ),
        style: Theme.of(context).textTheme.bodyText1,
        controller: controller,
        onChanged: (value) {
          context.read<EventFormCubit>().setTags(value);
        },
      ),
    );
  }
}

class _PriceField extends StatelessWidget {
  const _PriceField({Key? key, required this.controller}) : super(key: key);
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 10, top: 8, bottom: 8),
      child: TextField(
        decoration: InputDecoration(
          labelText: AppLocalizations.of(context)!.price,
        ),
        inputFormatters: [
          FilteringTextInputFormatter.allow(RegExp(r'[0-9]+[\.,]?[0-9]*')),
        ],
        keyboardType: TextInputType.number,
        style: Theme.of(context).textTheme.bodyText1,
        controller: controller,
        onChanged: (value) {
          context.read<EventFormCubit>().setPrice(value);
        },
      ),
    );
  }
}

class _MaxParticipantsField extends StatelessWidget {
  const _MaxParticipantsField({Key? key, required this.controller})
      : super(key: key);
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 20, left: 10, top: 8, bottom: 8),
      child: TextField(
        decoration: InputDecoration(
          labelText: AppLocalizations.of(context)!.maxParticipants,
        ),
        inputFormatters: [
          FilteringTextInputFormatter.digitsOnly,
        ],
        keyboardType: TextInputType.number,
        style: Theme.of(context).textTheme.bodyText1,
        controller: controller,
        onChanged: (value) {
          context.read<EventFormCubit>().setMaxParticipants(value);
        },
      ),
    );
  }
}

class _SendNotificationSwitch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: Row(
        children: [
          Expanded(
            child: Text(
              'Envoyer une notification à tous les utilisateurs',
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          BlocBuilder<EventFormCubit, EventFormState>(
            builder: (context, state) {
              return Switch(
                onChanged: (bool value) {
                  context.read<EventFormCubit>().setSendNotification(value);
                },
                activeColor: Theme.of(context).primaryColor,
                value: state.sendNotification,
              );
            },
          ),
        ],
      ),
    );
  }
}

class _NotificationTextField extends StatelessWidget {
  const _NotificationTextField({Key? key, required this.controller})
      : super(key: key);

  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<EventFormCubit, EventFormState>(
      builder: (context, state) {
        return state.sendNotification
            ? Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                child: TextField(
                  minLines: 3,
                  maxLines: null,
                  decoration: InputDecoration(
                    labelText: AppLocalizations.of(context)!.notificationText,
                    alignLabelWithHint: true,
                  ),
                  style: Theme.of(context).textTheme.bodyText1,
                  controller: controller,
                  onChanged: (value) {
                    context.read<EventFormCubit>().setNotificationText(value);
                  },
                ),
              )
            : Container();
      },
    );
  }
}
