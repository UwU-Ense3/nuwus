import 'package:ense3/UI/CustomWidgets/ErrorSnackbar.dart';
import 'package:ense3/UI/EventsTab/event_view_body.dart';
import 'package:ense3/event_creator/form/cubit/event_form_cubit.dart';
import 'package:ense3/models/global.dart';
import 'package:ense3/ressources/Event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class EventVerifPage extends StatelessWidget {
  const EventVerifPage({Key? key, required this.event, required this.editMode})
      : super(key: key);

  final bool editMode;
  final Event event;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(editMode
            ? AppLocalizations.of(context)!.editAnEvent
            : AppLocalizations.of(context)!.createAnEvent),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (editMode) {
            context.read<EventFormCubit>().updateEvent(event);
          } else {
            context.read<EventFormCubit>().createEvent(event);
          }
        },
        tooltip: editMode
            ? AppLocalizations.of(context)!.editAnEvent
            : AppLocalizations.of(context)!.createAnEvent,
        child: BlocConsumer<EventFormCubit, EventFormState>(
          listener: (context, state) {
            if (state is EventFormRequestFailed) {
              ErrorSnackbar.showSnackbar(context: context, text: state.message);
            } else if (state is EventFormRequestSuccess) {
              Navigator.of(context)
                ..pop()
                ..pop();
              snackBar(
                  context,
                  Icons.auto_awesome_rounded,
                  editMode
                      ? AppLocalizations.of(context)!
                          .eventHasBeenEdited(state.event.name)
                      : AppLocalizations.of(context)!
                          .eventHasBeenCreated(state.event.name));
            }
          },
          builder: (context, state) {
            if (state is EventFormLoading) {
              return CircularProgressIndicator(
                color: Theme.of(context).colorScheme.onPrimary,
              );
            }
            return Icon(
              Icons.send_rounded,
              color: Theme.of(context).colorScheme.onPrimary,
            );
          },
        ),
      ),
      body: BlocBuilder<EventFormCubit, EventFormState>(
        builder: (context, state) {
          return EventViewBody(
            event: state.event,
            isLocalImage: state.isLocalImage,
          );
        },
      ),
    );
  }
}
