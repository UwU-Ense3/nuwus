import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dart_date/dart_date.dart';
import 'package:ense3/authentication/login_bloc/LoginBloc.dart';
import 'package:ense3/event_creator/imgur/imgur_repository.dart';
import 'package:ense3/repository/EventRepository.dart';
import 'package:ense3/ressources/Event.dart';
import 'package:equatable/equatable.dart';
import 'package:file_picker/file_picker.dart';
import 'package:logger/logger.dart';
import 'package:universal_io/io.dart';

part 'event_form_state.dart';

class EventFormCubit extends Cubit<EventFormState> {
  EventFormCubit(
    this._loginBloc, {
    required bool sendNotification,
    required String apiUrl,
    Event? event,
    isLocalImage = true,
  })  : eventRepository = EventRepository(apiUrl: apiUrl),
        super(EventFormEditing(
          event: event,
          isLocalImage: isLocalImage,
          sendNotification: sendNotification,
          textNotification: '',
        ));
  final eventRepository;
  final _imgurRepository = ImgurRepository();
  final LoginBloc _loginBloc;

  void resetToDefault() {
    emit(EventFormEditing(
      isLocalImage: true,
      sendNotification: true,
      textNotification: '',
    ));
  }

  void setImage(Future<FilePickerResult?> future) async {
    var result = await future;
    if (result != null && result.files.isNotEmpty) {
      emit(EventFormEditing(
        event: state.event.copyWith(
          imageBytes: result.files.single.bytes,
          imageLink: null,
        ),
        isLocalImage: true,
        sendNotification: state.sendNotification,
        textNotification: state.notificationText,
      ));
    }
  }

  void setName(String name) {
    emit(EventFormEditing(
      event: state.event.copyWith(name: name),
      isLocalImage: state.isLocalImage,
      sendNotification: state.sendNotification,
      textNotification: state.notificationText,
    ));
  }

  void setDescription(String description) {
    emit(
      EventFormEditing(
        event: state.event.copyWith(description: description),
        isLocalImage: state.isLocalImage,
        sendNotification: state.sendNotification,
        textNotification: state.notificationText,
      ),
    );
  }

  void setBeginDateTime(String beginStr) {
    if (state.event.endTime.isBefore(state.event.beginTime)) {
      emit(EventFormEditing(
        event: state.event.copyWith(
          beginTime: DateTime.parse(beginStr),
          endTime: state.event.beginTime.add(const Duration(hours: 1)),
        ),
        isLocalImage: state.isLocalImage,
        sendNotification: state.sendNotification,
        textNotification: state.notificationText,
      ));
    }
    emit(EventFormEditing(
      event: state.event.copyWith(beginTime: DateTime.parse(beginStr)),
      isLocalImage: state.isLocalImage,
      sendNotification: state.sendNotification,
      textNotification: state.notificationText,
    ));
  }

  void setEndDateTime(String endStr) {
    emit(EventFormEditing(
      event: state.event.copyWith(endTime: DateTime.parse(endStr)),
      isLocalImage: state.isLocalImage,
      sendNotification: state.sendNotification,
      textNotification: state.notificationText,
    ));
  }

  void setOrganizers(String organizersStr) {
    emit(EventFormEditing(
      event: state.event.copyWith(
        organizers: _formatList(organizersStr, (e) => Organizer(0, e)),
      ),
      isLocalImage: state.isLocalImage,
      sendNotification: state.sendNotification,
      textNotification: state.notificationText,
    ));
  }

  void setLocation(String location) {
    emit(EventFormEditing(
      event: state.event.copyWith(location: location),
      isLocalImage: state.isLocalImage,
      sendNotification: state.sendNotification,
      textNotification: state.notificationText,
    ));
  }

  void setPrice(String priceStr) {
    int? newPriceInt;
    var newPriceDouble = double.tryParse(priceStr.replaceAll(',', '.'));
    if (newPriceDouble == null) {
      newPriceInt = null;
    } else {
      newPriceInt = (newPriceDouble * 100).round();
    }
    emit(EventFormEditing(
      event: state.event.copyWith(price: newPriceInt),
      isLocalImage: state.isLocalImage,
      sendNotification: state.sendNotification,
      textNotification: state.notificationText,
    ));
  }

  void setMaxParticipants(String maxParticipants) {
    emit(EventFormEditing(
      event: state.event
          .copyWith(numberParticipants: int.tryParse(maxParticipants)),
      isLocalImage: state.isLocalImage,
      sendNotification: state.sendNotification,
      textNotification: state.notificationText,
    ));
  }

  void setTags(String tagsStr) {
    emit(EventFormEditing(
      event: state.event.copyWith(
        tags: _formatList(tagsStr, (e) => Tag(0, e, '0xFFFFFF')),
      ),
      isLocalImage: state.isLocalImage,
      sendNotification: state.sendNotification,
      textNotification: state.notificationText,
    ));
  }

  void setSendNotification(bool sendNotification) {
    emit(EventFormEditing(
      event: state.event,
      isLocalImage: state.isLocalImage,
      sendNotification: sendNotification,
      textNotification: state.notificationText,
    ));
  }

  void setNotificationText(String textNotification) {
    emit(EventFormEditing(
      event: state.event,
      isLocalImage: state.isLocalImage,
      sendNotification: state.sendNotification,
      textNotification: textNotification,
    ));
  }

  void createEvent(Event event) async {
    emit(EventFormLoading(
      event: state.event,
      isLocalImage: state.isLocalImage,
      sendNotification: state.sendNotification,
      textNotification: state.notificationText,
    ));
    if (_loginBloc.state is LoggedIn) {
      if (event.imageBytes != null) {
        try {
          var link = await _imgurRepository.uploadImage(event.imageBytes!);
          event = event.copyWith(imageLink: link);
        } catch (e) {
          Logger().e('Failed to upload the image to imgur: $e');
          event = event.copyWith(imageLink: null);
        }
      }
      var mail = (_loginBloc.state as LoggedIn).user.mail;
      var token = (_loginBloc.state as LoggedIn).user.authToken;
      try {
        await eventRepository.createEvent(
          event,
          mail,
          token,
          sendNotification: state.sendNotification,
          notificationText: state.notificationText,
        );
        emit(EventFormRequestSuccess(
          event: state.event,
          isLocalImage: false,
          sendNotification: state.sendNotification,
          textNotification: state.notificationText,
        ));
      } on HttpException catch (e) {
        Logger().e(e);
        emit(EventFormRequestFailed(
          event: state.event,
          message: 'Connexion au serveur impossible : ${e.message}',
          isLocalImage: false,
          sendNotification: state.sendNotification,
          textNotification: state.notificationText,
        ));
      } on TimeoutException catch (e) {
        Logger().e(e);
        emit(EventFormRequestFailed(
          event: state.event,
          message: 'Connexion au serveur impossible ${e.message}',
          isLocalImage: false,
          sendNotification: state.sendNotification,
          textNotification: state.notificationText,
        ));
      } catch (e) {
        Logger().e(e);
        emit(EventFormRequestFailed(
          event: state.event,
          message: 'Erreur inconnue : $e',
          isLocalImage: false,
          sendNotification: state.sendNotification,
          textNotification: state.notificationText,
        ));
      }
    } else {
      emit(EventFormRequestFailed(
        event: state.event,
        message: 'Vous devez être connecté pour créer un événement',
        isLocalImage: true,
        sendNotification: state.sendNotification,
        textNotification: state.notificationText,
      ));
    }
  }

  void updateEvent(Event event) async {
    emit(EventFormLoading(
      event: state.event,
      isLocalImage: state.isLocalImage,
      sendNotification: state.sendNotification,
      textNotification: state.notificationText,
    ));
    if (_loginBloc.state is LoggedIn) {
      if (state.isLocalImage && event.imageBytes != null) {
        try {
          var link = await _imgurRepository.uploadImage(event.imageBytes!);
          event = event.copyWith(imageLink: link);
        } catch (e) {
          Logger().e('Failed to upload the image to imgur: $e');
          event = event.copyWith(imageLink: null);
        }
      }
      var mail = (_loginBloc.state as LoggedIn).user.mail;
      var token = (_loginBloc.state as LoggedIn).user.authToken;
      try {
        await eventRepository.editEvent(
          event.id,
          event,
          mail,
          token,
          sendNotification: state.sendNotification,
          notificationText: state.notificationText,
        );
        emit(EventFormRequestSuccess(
          event: state.event,
          isLocalImage: false,
          sendNotification: state.sendNotification,
          textNotification: state.notificationText,
        ));
      } on HttpException catch (e) {
        Logger().e(e);
        emit(EventFormRequestFailed(
          event: state.event,
          message: 'Connexion au serveur impossible : ${e.message}',
          isLocalImage: false,
          sendNotification: state.sendNotification,
          textNotification: state.notificationText,
        ));
      } on TimeoutException catch (e) {
        Logger().e(e);
        emit(EventFormRequestFailed(
          event: state.event,
          message: 'Connexion au serveur impossible ${e.message}',
          isLocalImage: false,
          sendNotification: state.sendNotification,
          textNotification: state.notificationText,
        ));
      } catch (e) {
        Logger().e(e);
        emit(EventFormRequestFailed(
          event: state.event,
          message: 'Erreur inconnue : $e',
          isLocalImage: false,
          sendNotification: state.sendNotification,
          textNotification: state.notificationText,
        ));
      }
    } else {
      emit(EventFormRequestFailed(
        event: state.event,
        message: 'Vous devez être connecté pour modifier un événement',
        isLocalImage: state.isLocalImage,
        sendNotification: state.sendNotification,
        textNotification: state.notificationText,
      ));
    }
  }

  void deleteEvent(int eventId) async {
    var loginState = _loginBloc.state;
    if (loginState is LoggedIn) {
      var mail = (loginState).user.mail;
      var token = (loginState).user.authToken;
      try {
        await eventRepository.deleteEvent(eventId, mail, token);
        emit(EventFormRequestSuccess(
          isLocalImage: true,
          sendNotification: state.sendNotification,
          textNotification: state.notificationText,
        ));
      } on HttpException catch (e) {
        Logger().e(e);
        emit(EventFormRequestFailed(
          event: state.event,
          message: 'Connexion au serveur impossible : ${e.message}',
          isLocalImage: true,
          sendNotification: state.sendNotification,
          textNotification: state.notificationText,
        ));
      } on TimeoutException catch (e) {
        Logger().e(e);
        emit(EventFormRequestFailed(
          event: state.event,
          message: 'Connexion au serveur impossible ${e.message}',
          isLocalImage: true,
          sendNotification: state.sendNotification,
          textNotification: state.notificationText,
        ));
      } catch (e) {
        Logger().e(e);
        emit(EventFormRequestFailed(
          event: state.event,
          message: 'Erreur inconnue : $e',
          isLocalImage: true,
          sendNotification: state.sendNotification,
          textNotification: state.notificationText,
        ));
      }
    } else {
      emit(EventFormRequestFailed(
        event: state.event,
        message: 'Vous devez être connecté pour supprimer un événement',
        isLocalImage: state.isLocalImage,
        sendNotification: state.sendNotification,
        textNotification: state.notificationText,
      ));
    }
  }

  List<T> _formatList<T>(String? text, T Function(String) constructor) {
    return ((text ?? '').split(',').where((element) {
      return element.trim() != '';
    }).map((element) {
      return constructor(element.trim());
    }).toList());
  }

// @override
// void onChange(Change<EventFormState> change) {
//   Logger().d(change.nextState);
//   super.onChange(change);
// }
}
