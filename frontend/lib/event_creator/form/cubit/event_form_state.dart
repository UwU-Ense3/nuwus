part of 'event_form_cubit.dart';

abstract class EventFormState {
  EventFormState(
      {required this.isLocalImage,
      required this.sendNotification,
      required this.notificationText,
      Event? event})
      : event = event ??
            Event(
              id: 0,
              name: '',
              beginTime:
                  DateTime.now().startOfHour.add(const Duration(hours: 1)),
              endTime: DateTime.now().startOfHour.add(const Duration(hours: 3)),
              location: '',
              organizers: const [],
              description: '',
              tags: const [],
              numberParticipants: 0,
              price: 0,
            );

  final Event event;
  final bool sendNotification;
  final String notificationText;
  final bool isLocalImage;
}

class EventFormEditing extends EventFormState with EquatableMixin {
  EventFormEditing({
    Event? event,
    required bool isLocalImage,
    required sendNotification,
    required textNotification,
  }) : super(
          event: event,
          isLocalImage: isLocalImage,
          sendNotification: sendNotification,
          notificationText: textNotification,
        );

  // EventFormEditing copyWith({Event? event, bool? isLocalImage}) {
  //   return EventFormEditing(
  //     event: event ?? this.event,
  //     isLocalImage: isLocalImage ?? this.isLocalImage,
  //   );

  @override
  List<Object> get props =>
      [event, isLocalImage, sendNotification, notificationText];
}

class EventFormLoading extends EventFormState with EquatableMixin {
  EventFormLoading({
    event,
    required isLocalImage,
    required sendNotification,
    required textNotification,
  }) : super(
          event: event,
          isLocalImage: isLocalImage,
          sendNotification: sendNotification,
          notificationText: textNotification,
        );

  @override
  List<Object> get props =>
      [event, isLocalImage, sendNotification, notificationText];
}

class EventFormRequestFailed extends EventFormState with EquatableMixin {
  final String message;

  EventFormRequestFailed({
    Event? event,
    required bool isLocalImage,
    required this.message,
    required sendNotification,
    required textNotification,
  }) : super(
          event: event,
          isLocalImage: isLocalImage,
          sendNotification: sendNotification,
          notificationText: textNotification,
        );

  @override
  List<Object> get props =>
      [event, message, isLocalImage, sendNotification, notificationText];
}

class EventFormRequestSuccess extends EventFormState with EquatableMixin {
  EventFormRequestSuccess({
    Event? event,
    required bool isLocalImage,
    required sendNotification,
    required textNotification,
  }) : super(
          event: event,
          isLocalImage: isLocalImage,
          sendNotification: sendNotification,
          notificationText: textNotification,
        );

  @override
  List<Object> get props =>
      [event, isLocalImage, sendNotification, notificationText];
}
