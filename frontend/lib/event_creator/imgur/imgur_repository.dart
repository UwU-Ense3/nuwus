import 'dart:convert';
import 'dart:typed_data';

import 'imgur_helper.dart';

class ImgurRepository {
  final _client = ImgurHelper();

  Future<String> uploadImage(Uint8List bytes) async {
    String base64Image = base64Encode(bytes);
    var response = await _client.post('3/image', {'image': base64Image});
    String link = json.decode(response)['data']['link'];
    return link;
  }
}
