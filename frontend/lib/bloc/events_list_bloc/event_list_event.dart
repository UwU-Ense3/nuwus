part of 'event_list_bloc.dart';

@immutable
abstract class EventsListEvent {}

class ListUpdateRequested extends EventsListEvent {}

class FilterUpdateRequested extends EventsListEvent {
  final bool? showPast;
  final String? searchQuery;
  final List<Tag>? selectedTags;

  // final SortOrder sortOrder;

  FilterUpdateRequested({this.selectedTags, this.showPast, this.searchQuery});
}

class SortingUpdateRequested extends EventsListEvent {
  final SortOrder sortOrder;

  SortingUpdateRequested({required this.sortOrder});
}

class EventViewed extends EventsListEvent {
  final int eventId;

  EventViewed(this.eventId);
}
