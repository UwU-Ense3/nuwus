import 'package:bloc/bloc.dart';
import 'package:ense3/bloc/events_list_bloc/visibility_filter.dart';
import 'package:ense3/repository/EventRepository.dart';
import 'package:ense3/ressources/Event.dart';
import 'package:logger/logger.dart';
import 'package:meta/meta.dart';

part 'event_list_event.dart';
part 'event_list_state.dart';

class EventsListBloc extends Bloc<EventsListEvent, EventsListState> {
  EventsListBloc({required String apiUrl})
      : _eventRepo = EventRepository(apiUrl: apiUrl),
        _tagRepo = TagRepository(apiUrl: apiUrl),
        super(EventsListInitial()) {
    on<ListUpdateRequested>(_onListUpdateRequested);
    on<FilterUpdateRequested>(_onFilterUpdateRequested);
    on<SortingUpdateRequested>(_onSortingUpdateRequested);
    on<EventViewed>(_onEventViewed);
  }

  final EventRepository _eventRepo;
  final TagRepository _tagRepo;
  List<Event> eventList = [];
  List<Tag> tagList = [];
  VisibilityFilter filter = VisibilityFilter(
    showPast: false,
    rawSearchQuery: '',
    sortOrder: SortOrder.chronological,
    selectedTags: [],
  ); // Default filter

  void _onListUpdateRequested(
      ListUpdateRequested event, Emitter<EventsListState> emit) async {
    emit(EventsListLoadInProgress());
    try {
      eventList = await _eventRepo.fetchEventsList(hidePast: false);
    } on Exception catch (e, stack) {
      Logger().e('Erreur de connexion : $e');
      emit(EventsListHasError(e));
      return;
    }
    try {
      tagList = await _tagRepo.fetchTagsList();
    } on Exception catch (e, stack) {
      Logger().e(
        'Erreur de connexion : $e',
      );
      emit(EventsListHasError(e));
      return;
    }
    var filteredList = filter.applyFilteringTo(eventList);
    filter.applyOrderingTo(filteredList);
    emit(EventsListLoadSuccess(filteredList, tagList, filter));
  }

  void _onFilterUpdateRequested(
      FilterUpdateRequested event, Emitter<EventsListState> emit) async {
    if (state is EventsListInitial ||
        state is EventsListHasError ||
        state is EventsListLoadInProgress) {
      emit(EventsListLoadInProgress());
      try {
        eventList = await _eventRepo.fetchEventsList(hidePast: false);
      } on Exception catch (e, stack) {
        Logger().e('Erreur de connexion : $e');
        emit(EventsListHasError(e));
        return;
      }
      try {
        tagList = await _tagRepo.fetchTagsList();
      } on Exception catch (e, stack) {
        Logger().e('Erreur de connexion : $e');
        emit(EventsListHasError(e));
        return;
      }
    }
    filter = filter.copyWith(
      showPast: event.showPast,
      rawSearchQuery: event.searchQuery,
      selectedTags: event.selectedTags,
    );
    var filteredList = filter.applyFilteringTo(eventList);
    filter.applyOrderingTo(filteredList);
    emit(EventsListLoadSuccess(filteredList, tagList, filter));
  }

  void _onSortingUpdateRequested(
      SortingUpdateRequested event, Emitter<EventsListState> emit) async {
    if (state is EventsListInitial ||
        state is EventsListHasError ||
        state is EventsListLoadInProgress) {
      emit(EventsListLoadInProgress());
      try {
        eventList = await _eventRepo.fetchEventsList(hidePast: false);
      } on Exception catch (e, stack) {
        Logger().e('Erreur de connexion : $e');
        emit(EventsListHasError(e));
        return;
      }
      try {
        tagList = await _tagRepo.fetchTagsList();
      } on Exception catch (e, stack) {
        Logger().e('Erreur de connexion : $e');
        emit(EventsListHasError(e));

        return;
      }
    }
    filter = filter.copyWith(sortOrder: event.sortOrder);
    var filteredList = filter.applyFilteringTo(eventList);
    filter.applyOrderingTo(filteredList);
    emit(EventsListLoadSuccess(filteredList, tagList, filter));
  }

  void _onEventViewed(EventViewed event, Emitter<EventsListState> emit) async {
    _eventRepo.fetchEvent(event.eventId);
  }
}
