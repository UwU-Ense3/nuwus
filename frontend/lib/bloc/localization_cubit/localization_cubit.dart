import 'package:bloc/bloc.dart';
import 'package:ense3/models/localization_saver.dart';
import 'package:flutter/material.dart';

class LocalizationCubit extends Cubit<Locale> {
  final _localizationSaver = LocalizationSaver();

  LocalizationCubit(Locale initialLocale) : super(initialLocale);

  void switchTo(Locale locale) {
    _localizationSaver.saveLocale(locale.toString());
    emit(locale);
  }

  Future<void> loadSavedLocale() async {
    var savedLocale = await _localizationSaver.loadLocale();
    if (savedLocale != null){
      emit(Locale(savedLocale));
    }
  }
}
