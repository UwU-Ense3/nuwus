// Point d'entrée de l'application : la fonction main() se lance au démarrage de l'application

import 'dart:ui';

import 'package:ense3/UI/EventsTab/event_view_page.dart';
import 'package:ense3/app_config.dart';
import 'package:ense3/bloc/events_list_bloc/event_list_bloc.dart';
import 'package:ense3/bloc/localization_cubit/localization_cubit.dart';
import 'package:ense3/notifications/cubit/firebase_notif_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:logger/logger.dart';
import 'package:theme_mode_handler/theme_mode_handler.dart';
import 'package:universal_io/io.dart';

import 'UI/Home.dart';
import 'authentication/login_bloc/LoginBloc.dart';
import 'models/ThemeSaver.dart';
import 'models/global.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  runApp(
    AppConfig(
      flavorName: 'prod',
      apiBaseUrl: 'https://api.2uwu4u.club/',
      child: MultiBlocProvider(
        providers: [
          //Bloc provider pour stocker la langue de l'application
          BlocProvider(create: (BuildContext context) {
            var localizationCubit = LocalizationCubit(
                Locale(Platform.localeName.contains('fr') ? 'fr' : 'en'));
            Logger()
                .d('Device language: ${localizationCubit.state.toString()}');
            localizationCubit.loadSavedLocale();
            return localizationCubit;
          }),
          // Bloc provider pour stocker l'état d'authentification
          BlocProvider(
              create: (BuildContext context) => LoginBloc(
                  apiUrl: context
                      .findAncestorWidgetOfExactType<AppConfig>()!
                      .apiBaseUrl)
                ..add(AppLoaded())),
          // Bloc provider pour stocker l'état de la liste d'événements
          BlocProvider(
              create: (BuildContext context) => EventsListBloc(
                  apiUrl: context
                      .findAncestorWidgetOfExactType<AppConfig>()!
                      .apiBaseUrl)),
          BlocProvider(
              create: (BuildContext context) => FirebaseNotifCubit(
                  apiUrl: context
                      .findAncestorWidgetOfExactType<AppConfig>()!
                      .apiBaseUrl)),
        ],
        child: const MyApp(),
      ),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ThemeModeHandler(
        // Permet de gérer le thème, de le changer et de stocker l'état actuel du theme (dark ou light)
        manager: MyManager(),
        defaultTheme: ThemeMode.light,
        builder: (ThemeMode themeMode) {
          return BlocBuilder<LocalizationCubit, Locale>(
            builder: (context, locale) {
              return MaterialApp(
                debugShowCheckedModeBanner: false,
                localizationsDelegates: const [
                  AppLocalizations.delegate,
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                  GlobalCupertinoLocalizations.delegate,
                ],
                locale: locale,
                supportedLocales: AppLocalizations.supportedLocales,
                scrollBehavior: const MaterialScrollBehavior().copyWith(
                  dragDevices: {
                    PointerDeviceKind.mouse,
                    PointerDeviceKind.touch,
                    PointerDeviceKind.trackpad,
                  },
                ),
                title: 'NUwUs',
                themeMode: themeMode,
                darkTheme: ThemeColors.darkTheme,
                theme: ThemeColors.lightTheme,
                home: BlocConsumer<FirebaseNotifCubit, FirebaseNotifState>(
                  listener: (context, state) {
                    if (state is FirebaseNotifNotFoundError) {
                      snackBar(context, Icons.error,
                          AppLocalizations.of(context)!.eventDoesNotExist);
                      return;
                    }
                    if (state is FirebaseNotifNetworkError) {
                      snackBar(
                          context,
                          Icons.wifi_off_rounded,
                          AppLocalizations.of(context)!
                              .erreur_connexion_serveur);
                      return;
                    }
                    if (state is FirebaseNotifDataAvailable) {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => WillPopScope(
                            onWillPop: () async {
                              context.read<FirebaseNotifCubit>().reset();
                              Navigator.pop(context);
                              return Future.value(false);
                            },
                            child: EventViewPage(event: state.event),
                          ),
                        ),
                      );
                    }
                  },
                  builder: (context, state) {
                    if (state is FirebaseNotifLoading) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                    // if (state is FirebaseNotifDataAvailable) {
                    //   return WillPopScope(
                    //     onWillPop: () async {
                    //       context.read<FirebaseNotifCubit>().reset();
                    //       return Future.value(false);
                    //     },
                    //     child: EventViewPage(event: state.event),
                    //   );
                    // }
                    else {
                      return const Home();
                    }
                  },
                ),
              );
            },
          );
        });
  }
}
