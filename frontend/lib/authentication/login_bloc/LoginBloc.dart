/// Ce fichier permet de gérer l'état d'authentification. Il prend en entrée des
/// événements (définis dans le fichier LoginEvent) que l'on peut appeler
/// depuis n'importe quel endroit de l'application et met à jour parmi ceux
/// définis dans LoginState l'état en fonction de ces événements.
import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:ense3/models/AuthHelper.dart';
import 'package:ense3/models/AuthSaver.dart';
import 'package:ense3/networking/ApiHelper.dart';
import 'package:ense3/ressources/User.dart';
import 'package:logger/logger.dart';
import 'package:meta/meta.dart';
import 'package:universal_io/io.dart';

part 'LoginEvent.dart';
part 'LoginState.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc({required String apiUrl})
      : _authHelper = AuthHelper(baseUrl: apiUrl),
        super(UnknownState()) {
    on<AppLoaded>(_onAppLoaded);
    on<UserStartedLogin>(_onUserStartedLogin);
    on<UserCompletedPasswordless>(_onUserCompletedPasswordless);
    on<UserLogout>(_onUserLogout);
    on<UserRegister>(_onUserRegister);
  }

  final _authHelper;

  // Start of the application
  void _onAppLoaded(AppLoaded event, Emitter<LoginState> emit) async {
    emit(LoadingState());
    var loginCredentials = await AuthSaver.loadUser();
    var mail = loginCredentials[0];
    var userToken = loginCredentials[1];
    if ((mail == '' || mail == null) ||
        (userToken == '' || userToken == null)) {
      Logger().d('Pas d\'identifiants trouvés');
      emit(LoggedOut());
    } else {
      try {
        var user = await _authHelper.login(mail, userToken);
        Logger().d('User logged in : ${user.firstName}');
        emit(LoggedIn(user));
      } on HttpResponseException catch (e) {
        if (e.statusCode == 404) {
          emit(WaitingForRegister(cachedMail: mail));
        } else {
          Logger().e('erreur $e');
          AuthSaver.saveUser('', '');
          emit(LoggedOut());
        }
      } on TimeoutException catch (e) {
        Logger().e('Erreur de communication avec le serveur : $e');
        emit(ConnectionError(e, cachedMail: mail, cachedToken: userToken));
      }
    }
  }

  // When user logs in
  Future<void> _onUserStartedLogin(
      UserStartedLogin event, Emitter<LoginState> emit) async {
    emit(
        LoadingState()); // affiche un écran de chargement pendant la communication avec le serveur
    var mail = event.mail.trim();
    try {
      await _authHelper.startLogin(mail);
      emit(WaitingForCode());
    } on TimeoutException catch (e) {
      Logger().e('Erreur de communication avec le serveur : $e');
      emit(ConnectionError(e, cachedMail: mail));
    } on HttpResponseException catch (e) {
      if (e.statusCode == 404) {
        emit(WaitingForRegister(cachedMail: mail));
      } else {
        Logger().d('erreur $e');
        AuthSaver.saveUser('', '');
        emit(ConnectionError(e));
      }
    }
  }

  // When user enters the code received by mail
  Future<void> _onUserCompletedPasswordless(
      UserCompletedPasswordless event, Emitter<LoginState> emit) async {
    var token = event.token.trim();
    try {
      User user = await _authHelper.passwordlessLogin(token);
      AuthSaver.saveUser(user.mail, user.authToken);
      Logger().d('User logged in : ${user.firstName}');
      emit(LoggedIn(user));
    } on TimeoutException catch (e) {
      Logger().e('Erreur de communication avec le serveur : $e');
      emit(ConnectionError(e));
    } on HttpException catch (e) {
      Logger().e('Erreur dans la requête : ${e.toString()}');
      emit(ConnectionError(e));
    }
  }

  // when user logs out
  Future<void> _onUserLogout(UserLogout event, Emitter<LoginState> emit) async {
    emit(LoadingState());
    await AuthSaver.saveUser('', '');
    emit(LoggedOut());
  }

  // when user registers
  Future<void> _onUserRegister(
      UserRegister event, Emitter<LoginState> emit) async {
    emit(LoadingState());
    try {
      await _authHelper.register(event.mail, event.firstName, event.lastName);
      emit(WaitingForCode());
    } on TimeoutException catch (e) {
      Logger().e('Erreur de communication avec le serveur : $e');
      emit(ConnectionError(
        e,
        cachedMail: event.mail,
      ));
    } on HttpException catch (e) {
      Logger().e('Erreur dans la requête : ${e.toString()}');
      emit(LoggedOut());
    }
  }
}
