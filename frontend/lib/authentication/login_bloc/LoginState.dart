part of 'LoginBloc.dart';

@immutable
abstract class LoginState {}

class UnknownState extends LoginState {}

class LoggedIn extends LoginState {
  final User user;

  LoggedIn(this.user);
}

class LoggedOut extends LoginState {}

class WaitingForCode extends LoginState {}

class WaitingForRegister extends LoginState {
  final String? cachedMail;

  WaitingForRegister({this.cachedMail});
}

class ConnectionError extends LoginState {
  final Exception exception;
  final String? cachedMail;
  final String? cachedToken;

  ConnectionError(this.exception, {this.cachedMail, this.cachedToken});
}

class LoadingState extends LoginState {}
