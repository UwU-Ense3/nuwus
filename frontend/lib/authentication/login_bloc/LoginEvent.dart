part of 'LoginBloc.dart';

@immutable
abstract class LoginEvent {}

class AppLoaded extends LoginEvent {}

class UserStartedLogin extends LoginEvent {
  final String mail;

  UserStartedLogin(this.mail);
}

class UserRegister extends LoginEvent {
  final String mail;
  final String firstName;
  final String lastName;

  UserRegister(this.mail, this.firstName, this.lastName);
}

class UserCompletedPasswordless extends LoginEvent {
  final String token;

  UserCompletedPasswordless(this.token);
}

class UserLogout extends LoginEvent {}