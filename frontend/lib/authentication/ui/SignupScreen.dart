import 'dart:core';

import 'package:ense3/models/global.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../login_bloc/LoginBloc.dart';

class SignupScreen extends StatefulWidget {
  final String mail;

  const SignupScreen({Key? key, this.mail = ''}) : super(key: key);

  @override
  State<SignupScreen> createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  final _mailController = TextEditingController();
  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    LoginState state = BlocProvider.of<LoginBloc>(context).state;
    if (state is ConnectionError) {
      _mailController.text = state.cachedMail ?? '';
      // _firstNameController.text = state.cachedUser.firstName;
      // _lastNameController.text = state.cachedUser.lastName;
    } else {
      final regex = RegExp(
          r"([a-z!#$%&'*+\/=?^_`{|}~-]+)\.([a-z!#$%&'*+\/=?^_`{|}~-]+).*@");
      final match = regex.firstMatch(widget.mail);
      if (match != null) {
        var firstName = match.group(1)!;
        var lastName = match.group(2)!;
        firstName = firstName[0].toUpperCase() + firstName.substring(1);
        lastName = lastName[0].toUpperCase() + lastName.substring(1);
        _firstNameController.text = firstName;
        _lastNameController.text = lastName;
      }
      _mailController.text = widget.mail;
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('NUwUs'),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: ScrollConfiguration(
          behavior: NoGlow(),
          child: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: Text(
                  AppLocalizations.of(context)!.creer_compte,
                  style: TextStyle(
                    color: Theme.of(context).textTheme.bodyText1!.color,
                    fontSize: 25,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15),
                child: Text(AppLocalizations.of(context)!.premiere_connexion,
                    style: Theme.of(context).textTheme.bodyText1),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: Row(
                  children: [
                    Flexible(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.only(right: 4.0),
                        child: TextField(
                          controller: _firstNameController,
                          maxLines: 1,
                          style: TextStyle(
                            color: Theme.of(context).textTheme.bodyText1!.color,
                          ),
                          autocorrect: false,
                          textInputAction: TextInputAction.next,
                          decoration: InputDecoration(
                            filled: true,
                            labelText: AppLocalizations.of(context)!.prenom,
                            // border: OutlineInputBorder(),
                          ),
                        ),
                      ),
                    ),
                    Flexible(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 4.0),
                        child: TextField(
                          controller: _lastNameController,
                          maxLines: 1,
                          style: TextStyle(
                            color: Theme.of(context).textTheme.bodyText1!.color,
                          ),
                          autocorrect: false,
                          textInputAction: TextInputAction.next,
                          decoration: InputDecoration(
                            filled: true,
                            labelText: AppLocalizations.of(context)!.nom,
                            // border: OutlineInputBorder(),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: TextField(
                  controller: _mailController,
                  keyboardType: TextInputType.emailAddress,
                  style: TextStyle(
                    color: Theme.of(context).textTheme.bodyText1!.color,
                  ),
                  autocorrect: false,
                  onSubmitted: (_) {
                    _submitCode(context);
                  },
                  decoration: InputDecoration(
                    filled: true,
                    labelText: AppLocalizations.of(context)!.adresse,
                    // border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: ButtonBar(
                  children: [
                    OutlinedButton(
                      style: Theme.of(context).outlinedButtonTheme.style,
                      onPressed: () {
                        BlocProvider.of<LoginBloc>(context).add(AppLoaded());
                      },
                      child: Text(
                        AppLocalizations.of(context)!.retour,
                      ),
                    ),
                    ElevatedButton(
                      style: Theme.of(context).outlinedButtonTheme.style,
                      onPressed: () {
                        _submitCode(context);
                      },
                      child: Text(
                        AppLocalizations.of(context)!.creer_compte,
                        style: TextStyle(
                            color: Theme.of(context).colorScheme.onPrimary),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _submitCode(BuildContext context) {
    BlocProvider.of<LoginBloc>(context).add(
      UserRegister(
        _mailController.text,
        _firstNameController.text,
        _lastNameController.text,
      ),
    );
  }
}
