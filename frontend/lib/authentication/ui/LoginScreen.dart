import 'package:ense3/models/global.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../login_bloc/LoginBloc.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _mailController = TextEditingController();
  String? _validation;

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    _mailController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    LoginState state = BlocProvider.of<LoginBloc>(context).state;

    if (state is ConnectionError) {
      _mailController.text = state.cachedMail ?? '';
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('NUwUs'),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: ScrollConfiguration(
          behavior: NoGlow(),
          child: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: Text(
                  AppLocalizations.of(context)!.connexion,
                  style: TextStyle(
                    color: Theme.of(context).textTheme.bodyText1!.color,
                    fontSize: 25,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15.0),
                child: Text(
                  AppLocalizations.of(context)!.message_connexion,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: TextField(
                  keyboardType: TextInputType.emailAddress,
                  controller: _mailController,
                  style: TextStyle(
                    color: Theme.of(context).textTheme.bodyText1!.color,
                  ),
                  autocorrect: false,
                  onSubmitted: (_) {
                    _submitMail(context);
                  },
                  decoration: InputDecoration(
                    filled: true,
                    labelText: AppLocalizations.of(context)!.adresse,
                    errorText: _validation,
                    // border: OutlineInputBorder(),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: ElevatedButton(
                  style: Theme.of(context).elevatedButtonTheme.style,
                  onPressed: () {
                    _submitMail(context);
                  },
                  child: Text(
                    AppLocalizations.of(context)!.continuer,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _submitMail(BuildContext context) {
    setState(() {
      _validation = _validateEmail(_mailController.text, context);
    });

    if (_validation == null) {
      BlocProvider.of<LoginBloc>(context)
          .add(UserStartedLogin(_mailController.text));
    }
  }

  String? _validateEmail(String mail, BuildContext context) {
    /// Renvoie null si l'adresse mail donnée est valide selon http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
    /// sinon, renvoie une phrase expliquant pourquoi l'adresse n'est pas valide.
    ///
    if (mail == '') {
      return AppLocalizations.of(context)!.entrer_email;
    }
    final regex = RegExp(
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$");
    if (!regex.hasMatch(mail.trim())) {
      return AppLocalizations.of(context)!.entrer_email_valide;
    }
    return null;
  }
}
