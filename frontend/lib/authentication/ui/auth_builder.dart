import 'package:ense3/UI/CustomWidgets/ErrorSnackbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../login_bloc/LoginBloc.dart';
import 'LoginScreen.dart';
import 'PasswordlessCodeScreen.dart';
import 'SignupScreen.dart';

class AuthBuilder extends StatelessWidget {
  const AuthBuilder({Key? key, required this.child}) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(
      listener: (context, state) {
        if (state is ConnectionError) {
          ErrorSnackbar.showSnackbar(
            context: context,
            text: 'Erreur de connexion au serveur',
          );
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(
        // Permet d'afficher différentes pages en fonction de l'état d'authentification
        builder: (BuildContext context, LoginState state) {
          // Quand on démarre l'application
          if (state is UnknownState) {
            BlocProvider.of<LoginBloc>(context).add(AppLoaded());
            return Scaffold(
              body: Center(child: Container()),
            );
          }
          // Si un utilisateur est en train de recevoir le code de vérification par mail
          if (state is WaitingForCode) {
            return const PasswordlessCodeScreen();
          }
          // Si un utilisateur est connecté
          if (state is LoggedIn) {
            if (state.user.canCreateEvent) {
              return child;
            } else {
              return Scaffold(
                body: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(30.0),
                        child: Text(
                          AppLocalizations.of(context)!
                              .connectedButNotAuthorized,
                          style: Theme.of(context).textTheme.bodyText2,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      ElevatedButton(
                        child: Text(
                          'Retour',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(
                                  color:
                                      Theme.of(context).colorScheme.onPrimary),
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                ),
              );
            }
          }
          // Si un utilisateur doit register
          if (state is WaitingForRegister) {
            return SignupScreen(mail: state.cachedMail ?? '');
          }
          // Si l'utilisateur est déconnecté
          if (state is LoggedOut) {
            return const LoginScreen();
          }
          // Etat de transition pendant l'attente du chargement durant un login ou register
          if (state is LoadingState) {
            return const Scaffold(
              body: Center(child: CircularProgressIndicator()),
            );
          }
          // Si il y a une erreur de connexion
          if (state is ConnectionError) {
            // à modifier plus tard
            return const LoginScreen();
          } else {
            throw (UnimplementedError);
          }
        },
      ),
    );
  }
}
