import 'package:ense3/models/global.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../login_bloc/LoginBloc.dart';

class PasswordlessCodeScreen extends StatefulWidget {
  const PasswordlessCodeScreen({Key? key}) : super(key: key);

  @override
  State<PasswordlessCodeScreen> createState() => _PasswordlessCodeScreenState();
}

class _PasswordlessCodeScreenState extends State<PasswordlessCodeScreen> {
  final TextEditingController _codeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('NUwUs'),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: ScrollConfiguration(
          behavior: NoGlow(),
          child: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: Text(
                  AppLocalizations.of(context)!.verif_mail,
                  style: TextStyle(
                    color: Theme.of(context).textTheme.bodyText1!.color,
                    fontSize: 25,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15.0),
                child: Text(
                  AppLocalizations.of(context)!.recevoir_code_mail,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: TextField(
                  controller: _codeController,
                  maxLines: 1,
                  style: TextStyle(
                    color: Theme.of(context).textTheme.bodyText1!.color,
                  ),
                  autocorrect: false,
                  decoration: InputDecoration(
                    filled: true,
                    labelText: AppLocalizations.of(context)!.code_verif,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: ElevatedButton(
                  style: Theme.of(context).elevatedButtonTheme.style,
                  onPressed: () {
                    BlocProvider.of<LoginBloc>(context)
                        .add(UserCompletedPasswordless(_codeController.text));
                  },
                  child: Text(
                    AppLocalizations.of(context)!.se_connecter,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: OutlinedButton(
                  style: Theme.of(context).outlinedButtonTheme.style,
                  onPressed: () {
                    BlocProvider.of<LoginBloc>(context).add(UserLogout());
                  },
                  child: Text(
                    AppLocalizations.of(context)!.retour,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
