import os
import platform

import connexion
# from flask_cors import CORS
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

basedir = os.path.abspath(os.path.dirname(__file__))
db_file = os.path.join(basedir, "db", "nuwus.db")
# Create the connexion application instance
connex_app = connexion.App(__name__, specification_dir=basedir)

# Get the underlying Flask app instance
app = connex_app.app
# CORS(app, resources={r"/api/*": {"origins": "*"}})

# Build the Sqlite ULR for SqlAlchemy
# L'url dépend du système d'expoitation, on detecte donc sur quel OS le server est lancé pour
# faire la config :
if platform.system() == 'Windows':
    sqlite_url = "sqlite:///" + db_file
else:
    sqlite_url = "sqlite:////" + db_file


# Configure the SqlAlchemy part of the app instance
app.config["SQLALCHEMY_ECHO"] = False
app.config["SQLALCHEMY_DATABASE_URI"] = sqlite_url
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

# Create the SqlAlchemy db instance
db = SQLAlchemy(app)

# Initialize Marshmallow
ma = Marshmallow(app)
