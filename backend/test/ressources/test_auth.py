from unittest.mock import patch, ANY

from resources.auth import *


@patch('resources.auth.client')
def test_start_login(client_mock):
    """
    Test if the start_login function successfully send the passwordless code and return 200 (ok) or 404 (user doesn't
    exist)
    """
    client_mock.start_passwordless_login.return_value.response.status_code = 200
    client_mock.start_passwordless_login.return_value.response.json.return_value = {
        "code": "ABDIJI"
        }
    client_mock.send_passwordless_code.return_value.response.status_code = 200

    result = start_login("simardced31@gmail.com")
    client_mock.start_passwordless_login.assert_called_once_with({
        "applicationId": application_id,
        "loginId": "simardced31@gmail.com"
        })
    assert result == ({}, 200)

    client_mock.start_passwordless_login.return_value.response.status_code = 404
    result = start_login("unknown@mail.com")
    assert result == ({}, 404)


@patch('resources.auth._get_user_from_login_response')
@patch('resources.auth.client')
def test_passwordless_login_success(client_mock, _get_user_from_login_response_mock):
    """
    Test the passwordless_login function with a successful login
    """
    client_mock.passwordless_login.return_value.response.status_code = 200
    client_mock.passwordless_login.return_value.response.json.return_value = {
        'token': 't072p1Niv7pDZ1xXUqH0KxGETNjP.88LF8biFhz6IV8mPMQl8SybI9aE1wHKx18BwOR47s23Dnd8WkHwcdgHpnTH3AMOgkT2i0SR7'
                 'GyM3288Z1IDMUaN38rbqj44.3tBatQJyNOxtuqZqfr6ApjUYZt5VV1l5BDlrY6zsunIDfQMa4Y6sCf40yq9aoiFgC75ot0nOyf083'
                 'BsZvwbV9MZAKJVox.2iRmonkpV.UW9k00frBA9h9Si2nD83as6hlDK5rUmaDCnGVS2cmVVK7ZihyDyfhiYqU6oBa7INJKqtRSeIxE'
                 'uJoLD7.skb.wbVqliFeTsf4I8J5YmqDldv94DLEX7zbEFrl32UKNkBLXM9l4pcVWuDT6eEQjxWkP852zcKem47FWxMOIjmDT7mAhk'
                 'tCN2jjORM.gpKAQLyT5czc7O1k7JJfvAQSylXwWUXiVk9J0FHCcmEIKD9UVT.nkKaDFYYEYtfQsKtWGCAfKZmcXU3xFnvt5I3j009'
                 'HPKlDa5dvkKdWfzvFxtdc7XdfFCCiEnjNy4TcplPXsEL4EAzo9mjR1uRvvrFNPGCv9zzaUw8aJ2EZ2.IjZUOhOcDPAmz8FfYlatId'
                 'p3QwWsL80tvCuNCt8Wa7TygFP1SteiGesFiDu981RjishvSecyDxAYmeh',
        'tokenExpirationInstant': 16256656666262,
        'user': {
            'active': True,
            'connectorId': 'CPR8LtBtmeWy4OsIDn-Q848zMIY3GZOoOy8F',
            'email': 'simardced31@gmail.com',
            'firstName': 'Cédric',
            'id': 'h2agyLJwZMnRyyiA7MiNGJiA2F9v3pmzVmn',
            'insertInstant': 16164344614,
            'lastLoginInstant': 46376434642,
            'lastName': 'Simard',
            'lastUpdateInstant': 1659262204535,
            'passwordChangeRequired': False,
            'passwordLastUpdateInstant': 1659262204632,
            'registrations': [{
                'applicationId': 'LZjl0bVbWO8Qum7nyDhg6tcVaZcvSu0SpoKt',
                'id': 'zQ4w56J3UlOZ6lnfb5-KrG5J8ak5OOwBHj0d',
                'insertInstant': 1659262204644,
                'lastLoginInstant': 1662999077282,
                'lastUpdateInstant': 1659262204644,
                'roles': ['admin'],
                'usernameStatus': 'ACTIVE',
                'verified': True
                }, {
                'applicationId': application_id,
                'authenticationToken': 'GnqX2ZIfc-5goUqzzc4OEd8lQZJ-QSIcoKJKe017VTy',
                'id': 'rw9dp1q-82n9x28rogdplvgri54f1qwxfuxs',
                'insertInstant': 1659263589016,
                'lastLoginInstant': 1663005825385,
                'lastUpdateInstant': 1660830399898,
                'roles': ['admin'],
                'usernameStatus': 'ACTIVE',
                'verified': True
                }],
            'tenantId': 'ewq2efj560dzz4r796cwn2n5nyzqeolq3loa',
            'twoFactor': {},
            'usernameStatus': 'ACTIVE',
            'verified': True
            }
        }

    _get_user_from_login_response_mock.return_value = User(id=1, mail='simardced31@gmail.com', first_name='Cédric',
                                                           last_name='Simard')
    code = "CODE"
    result = passwordless_login(code)
    client_mock.passwordless_login.assert_called_once_with({
        "code": code
        })
    assert result[0] == {
        'id': 1,
        'mail': 'simardced31@gmail.com',
        'last_name': 'Simard',
        'first_name': 'Cédric',
        'token': 'GnqX2ZIfc-5goUqzzc4OEd8lQZJ-QSIcoKJKe017VTy',
        'can_create_event': True,
        'can_modify_all_events': True
        }
    assert result[1] == 200


@patch('resources.auth._get_user_from_login_response')
@patch('resources.auth.client')
def test_passwordless_login_not_registered(client_mock, _get_user_from_login_response_mock):
    """
    Test login of a user that is not registered in the application
    """
    client_mock.passwordless_login.return_value.response.status_code = 202
    client_mock.passwordless_login.return_value.response.json.return_value = {
        'user': {
            'email': 'simardced31@gmail.com',
            'firstName': 'Cédric',
            'lastName': 'Simard',
            'id': 'h2agyLJwZMnRyyiA7MiNGJiA2F9v3pmzVmn',
            'registrations': [{
                'applicationId': 'LZjl0bVbWO8Qum7nyDhg6tcVaZcvSu0SpoKt',
                'roles': ['admin']
                }]
            }
        }
    client_mock.register.return_value.response.status_code = 200
    client_mock.register.return_value.response.json.return_value = {
        'registration': {
            'applicationId': application_id,
            'authenticationToken': 'GnqX2ZIfc-5goUqzzc4OEd8lQZJ-QSIcoKJKe017VTy',
            'id': 'h2agyLJwZMnRyyiA7MiNGJiA2F9v3pmzVmn',
            'roles': ['default'],
            },
        }
    _get_user_from_login_response_mock.return_value = User(id=1, mail='simardced31@gmail.com', first_name='Cédric',
                                                           last_name='Simard')
    code = "CODE"
    result = passwordless_login(code)
    client_mock.passwordless_login.assert_called_once_with({
        "code": code
        })
    client_mock.register.assert_called_once_with(user_id='h2agyLJwZMnRyyiA7MiNGJiA2F9v3pmzVmn', request={
        "generateAuthenticationToken": True,
        "registration": {
            "applicationId": application_id,
            "roles": ["default"]
            }
        })

    assert result[0] == {
        'id': 1,
        'mail': 'simardced31@gmail.com',
        'last_name': 'Simard',
        'first_name': 'Cédric',
        'token': 'GnqX2ZIfc-5goUqzzc4OEd8lQZJ-QSIcoKJKe017VTy',
        'can_create_event': False,
        'can_modify_all_events': False
        }
    assert result[1] == 202


@patch('resources.auth._get_user_from_login_response')
@patch('resources.auth.client')
def test_passwordless_login_no_registration(client_mock, _get_user_from_login_response_mock):
    """
    Test login of a user that is not registered in any application
    """
    client_mock.passwordless_login.return_value.response.status_code = 202
    client_mock.passwordless_login.return_value.response.json.return_value = {
        'user': {
            'email': 'simardced31@gmail.com',
            'firstName': 'Cédric',
            'lastName': 'Simard',
            'id': 'h2agyLJwZMnRyyiA7MiNGJiA2F9v3pmzVmn',
            }
        }
    client_mock.register.return_value.response.status_code = 200
    client_mock.register.return_value.response.json.return_value = {
        'registration': {
            'applicationId': application_id,
            'authenticationToken': 'GnqX2ZIfc-5goUqzzc4OEd8lQZJ-QSIcoKJKe017VTy',
            'id': 'h2agyLJwZMnRyyiA7MiNGJiA2F9v3pmzVmn',
            'roles': ['default'],
            },
        }
    _get_user_from_login_response_mock.return_value = User(id=1, mail='simardced31@gmail.com', first_name='Cédric',
                                                           last_name='Simard')
    code = "CODE"
    result = passwordless_login(code)
    client_mock.passwordless_login.assert_called_once_with({
        "code": code
        })
    assert result[0] == {
        'id': 1,
        'mail': 'simardced31@gmail.com',
        'last_name': 'Simard',
        'first_name': 'Cédric',
        'token': 'GnqX2ZIfc-5goUqzzc4OEd8lQZJ-QSIcoKJKe017VTy',
        'can_create_event': False,
        'can_modify_all_events': False
        }
    assert result[1] == 202


@patch('resources.auth.client')
def test_passwordless_login_no_token(client_mock):
    """
    Test login of a user that is registered in the application but doesn't have the authentication token generation
    activated
    """
    client_mock.passwordless_login.return_value.response.status_code = 200
    client_mock.passwordless_login.return_value.response.json.return_value = {
        'user': {
            'email': 'simardced31@gmail.com',
            'firstName': 'Cédric',
            'lastName': 'Simard',
            'id': 'h2agyLJwZMnRyyiA7MiNGJiA2F9v3pmzVmn',
            'registrations': [{
                'applicationId': 'LZjl0bVbWO8Qum7nyDhg6tcVaZcvSu0SpoKt',
                'roles': ['admin']
                }, {
                'applicationId': application_id,
                'roles': ['admin'],
                }]
            }
        }
    client_mock.patch_registration.return_value.response.status_code = 200
    client_mock.patch_registration.return_value.response.json.return_value = {
        'registration': {
            'applicationId': application_id,
            'authenticationToken': 'GnqX2ZIfc-5goUqzzc4OEd8lQZJ-QSIcoKJKe017VTy',
            }
        }
    # _get_user_from_login_response_mock.return_value = User(id=1, mail='simardced31@gmail.com', first_name='Cédric',
    #                                                        last_name='Simard')
    code = "CODE"
    result = passwordless_login(code)
    client_mock.passwordless_login.assert_called_once_with({
        "code": code
        })
    assert result[0] == {
        'id': 1,
        'mail': 'simardced31@gmail.com',
        'last_name': 'Simard',
        'first_name': 'Cédric',
        'token': 'GnqX2ZIfc-5goUqzzc4OEd8lQZJ-QSIcoKJKe017VTy',
        'can_create_event': True,
        'can_modify_all_events': True
        }
    assert result[1] == 200


@patch('resources.auth.client')
def test_passwordless_login_no_roles(client_mock):
    """
    Test passwordless login of a user that is registered in the application but doesn't have any roles
    """

    client_mock.passwordless_login.return_value.response.status_code = 200
    client_mock.passwordless_login.return_value.response.json.return_value = {
        'token': 'GnqX2ZIfc-5goUqzzc4OEd8lQZJ-QSIcoKJKe017VTy',
        'user': {
            'email': 'simardced31@gmail.com',
            'firstName': 'Cédric',
            'lastName': 'Simard',
            'id': 'h2agyLJwZMnRyyiA7MiNGJiA2F9v3pmzVmn',
            'registrations': [{
                'applicationId': 'LZjl0bVbWO8Qum7nyDhg6tcVaZcvSu0SpoKt',
                'roles': ['admin']
                }, {
                'applicationId': application_id,
                }]
            }
        }
    client_mock.patch_registration.return_value.response.status_code = 200
    client_mock.patch_registration.return_value.response.json.return_value = {
        'registration': {
            'applicationId': application_id,
            'authenticationToken': 'GnqX2ZIfc-5goUqzzc4OEd8lQZJ-QSIcoKJKe017VTy',
            }
        }

    code = "CODE"
    result = passwordless_login(code)
    client_mock.passwordless_login.assert_called_once_with({
        "code": code
        })
    assert result[0] == {
        'id': 1,
        'mail': 'simardced31@gmail.com',
        'last_name': 'Simard',
        'first_name': 'Cédric',
        'token': 'GnqX2ZIfc-5goUqzzc4OEd8lQZJ-QSIcoKJKe017VTy',
        'can_create_event': False,
        'can_modify_all_events': False
        }
    assert result[1] == 200


def test_passwordless_login_apple_review():
    """
    Test login of apple review user
    """
    code = "G9ewp&d*fYvmyEW9"
    result = passwordless_login(code)
    assert result == ({
                          "id": 9999999,
                          "mail": "foo@2uwu4u.club",
                          "first_name": "foo",
                          "last_name": "foo",
                          "token": "G9ewp&d*fYvmyEW9",
                          "can_create_event": True,
                          "can_modify_all_events": True
                          }, 200)


@patch('resources.auth.client')
@patch('resources.auth.start_login')
def test_register(start_login_mock, client_mock):
    """
    Test creation of a new user and registration in the application
    """
    start_login_mock.return_value = {}, 200
    client_mock.register.return_value.response.status_code = 200
    client_mock.register.return_value.response.json.return_value = {
        'registration': {
            'applicationId': application_id,
            'authenticationToken': 'GnqX2ZIfc-5goUqzzc4OEd8lQZJ-QSIcoKJKe017VTy',
            'id': 'h2agyLJwZMnRyyiA7MiNGJiA2F9v3pmzVmn',
            'roles': ['default'],
            },
        }

    mail = "create@mail.com"
    first_name = "Jean"
    last_name = "Michel"
    body = {
        'mail': mail,
        'first_name': first_name,
        'last_name': last_name,
        }
    result = register(body)
    client_mock.register.assert_called_once_with(request={
        "generateAuthenticationToken": True,
        "registration": {
            "applicationId": application_id,
            "roles": ["default"]
            },
        "user": {
            "email": mail,
            "lastName": last_name,
            "firstName": first_name,
            "password": ANY
            }
        })
    assert result == ({}, 200)
