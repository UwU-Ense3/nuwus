import random


def get_random_color(pastel_factor=0.5):
    return [(x + pastel_factor) / (1.0 + pastel_factor) for x in [random.uniform(0, 1.0) for i in [1, 2, 3]]]


def color_distance(c1, c2):
    return sum([abs(x[0] - x[1]) for x in zip(c1, c2)])


def generate_new_color(existing_colors, pastel_factor=0.5):
    existing_colors = [hextorgb(c) for c in existing_colors]
    max_distance = None
    best_color = None
    for i in range(0, 100):
        color = get_random_color(pastel_factor=pastel_factor)
        if not existing_colors:
            return rgbtohex(color)
        best_distance = min([color_distance(color, c) for c in existing_colors])
        if not max_distance or best_distance > max_distance:
            max_distance = best_distance
            best_color = color
    return rgbtohex(best_color)


def hextorgb(hex):
    hex = hex.lstrip('0xff')
    return tuple(int(hex[i:i + 2], 16) / 255.0 for i in (0, 2, 4))


def rgbtohex(rgb):
    return '0xff' + ''.join(f'{int(i * 255):02X}' for i in rgb)


# Example:

if __name__ == '__main__':

    # To make your color choice reproducible, uncomment the following line:
    random.seed(10)

    colors = []

    for i in range(0, 10):
        print(colors)
        new_color = generate_new_color(colors, pastel_factor=0.8)
        colors.append(new_color)

    print("Your colors:", colors)
