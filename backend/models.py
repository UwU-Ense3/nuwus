from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from marshmallow import fields

from config import db


# User
class User(db.Model):
    __tablename__ = "user"
    id = db.Column(db.Integer(), primary_key=True)
    mail = db.Column(db.String(80))
    last_name = db.Column(db.String(80))
    first_name = db.Column(db.String(80))


class UserSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = User
        load_instance = True


# Event
class Event(db.Model):
    __tablename__ = "event"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    begin_time = db.Column(db.DateTime)
    end_time = db.Column(db.DateTime)
    location = db.Column(db.String)
    description = db.Column(db.Text)
    organizers = db.relationship("Organizer", secondary='organize', cascade="all,delete")
    tags = db.relationship("Tag", secondary='tagmap', cascade="all,delete")
    n_participants = db.Column(db.Integer)
    image_link = db.Column(db.String)
    price = db.Column(db.Integer)
    number_views = db.Column(db.Integer, default=0)
    creation_time = db.Column(db.DateTime, default=db.func.now())
    update_time = db.Column(db.DateTime)
    created_by_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    created_by = db.relationship("User", foreign_keys=[created_by_id])
    updated_by_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    updated_by = db.relationship("User", foreign_keys=[updated_by_id])


class Tag(db.Model):
    __tablename__ = "tag"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    color = db.Column(db.String)
    events = db.relationship(Event, secondary='tagmap', viewonly=True)


class Organizer(db.Model):
    __tablename__ = "organizer"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    mail = db.Column(db.String)
    events = db.relationship(Event, secondary='organize', viewonly=True)


class Organize(db.Model):
    __tablename__ = "organize"
    event_id = db.Column(db.Integer, db.ForeignKey("event.id"), primary_key=True)
    organizer_id = db.Column(db.Integer, db.ForeignKey("organizer.id"), primary_key=True)


class TagMap(db.Model):
    __tablename__ = "tagmap"
    event_id = db.Column(db.Integer, db.ForeignKey("event.id"), primary_key=True)
    tag_id = db.Column(db.Integer, db.ForeignKey("tag.id"), primary_key=True)


class EventSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Event
        load_instance = True

    @staticmethod
    def serialize_created_by(event):
        if event.created_by is None:
            return None
        return event.created_by.first_name + " " + event.created_by.last_name

    @staticmethod
    def serialize_created_by_id(event):
        if event.created_by is None:
            return None
        return event.created_by_id

    @staticmethod
    def serialize_updated_by(event):
        if event.updated_by is None:
            return None
        return event.updated_by.first_name + " " + event.updated_by.last_name

    organizers = fields.Nested('OrganizerSchema', many=True, exclude=['mail'])
    tags = fields.Nested('TagSchema', many=True)
    created_by_id = fields.Function(serialize=lambda obj: EventSchema.serialize_created_by_id(obj))
    created_by = fields.Function(serialize=lambda obj: EventSchema.serialize_created_by(obj))
    updated_by = fields.Function(serialize=lambda obj: EventSchema.serialize_updated_by(obj))


class OrganizerSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Organizer
        load_instance = True


class TagSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Tag
        load_instance = True
