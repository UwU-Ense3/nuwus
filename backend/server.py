"""
Main module of the server file
"""

import argparse
import logging

from flask import render_template

# local modules
import config

# Get the application instance
from firebase import firebase_notifications

connex_app = config.connex_app

# Read the swagger.yml file to configure the endpoints
connex_app.add_api("swagger.yml")


# create a URL route in our application for "/"
@connex_app.route("/")
def home():
    """
    This function just responds to the browser URL
    localhost:5000/

    :return:        the rendered template "home.html"
    """
    return render_template("home.html")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--debug', action="store_true", help="Run the server in debug mode")
    args = parser.parse_args()
    firebase_notifications.init()
    if args.debug:
        connex_app.run(debug=True)
    else:
        import waitress as waitress
        logger = logging.getLogger('waitress')
        logger.setLevel(logging.INFO)
        waitress.serve(connex_app, host="0.0.0.0", port=5000, threads=6, )
