from dataclasses import dataclass, field
from enum import Enum
from typing import Optional

from config import db
from models import User
from secret import application_id


class Roles(Enum):
    DEFAULT = "default"
    MANAGER = "manager"
    ADMIN = "admin"


@dataclass
class FusionAuthUser:
    _db_id: Optional[int]
    fa_id: str
    mail: str
    first_name: str
    last_name: str
    token: Optional[str]
    roles: tuple[Roles]
    can_create_event: bool = field(init=False)
    can_modify_all_events: bool = field(init=False)

    def __post_init__(self):
        self.can_create_event = (Roles.MANAGER in self.roles) or (Roles.ADMIN in self.roles)
        self.can_modify_all_events = (Roles.ADMIN in self.roles)

    def get_id(self) -> int:
        if self._db_id is None:
            self._db_id = self.get_db_user().id
        return self._db_id

    @staticmethod
    def from_json(login_json) -> "FusionAuthUser":
        nuwus_registration = None
        if "registrations" in login_json["user"]:
            registrations = login_json["user"]["registrations"]
        else:
            registrations = []
        for registration in registrations:
            if registration["applicationId"] == application_id:
                nuwus_registration = registration
        if nuwus_registration is not None:
            token = nuwus_registration.get('authenticationToken')
            if nuwus_registration.get('roles') is not None:
                roles = tuple(Roles(role) for role in nuwus_registration["roles"])
            else:
                roles = tuple()
        else:
            token = None
            roles = tuple()
        return FusionAuthUser(mail=login_json['user']['email'], first_name=login_json['user']['firstName'],
                              last_name=login_json['user']['lastName'], token=token, roles=roles, _db_id=None,
                              fa_id=login_json['user']['id'])

    def patch_from_register_json(self, register_json):
        self.token = register_json["registration"]["authenticationToken"]
        if "roles" in register_json["registration"]:
            self.roles = tuple(Roles(role) for role in register_json["registration"]["roles"])
        self.__post_init__()

    def get_db_user(self) -> "User":
        user = User(mail=self.mail, first_name=self.first_name, last_name=self.last_name)
        existing_user = User.query.filter(User.mail == user.mail).one_or_none()
        if existing_user is None:
            db.session.add(user)
            db.session.commit()
            return User.query.filter(User.mail == user.mail).one()
        return existing_user

    def to_json(self) -> dict[str, object]:
        if self._db_id is None:
            self._db_id = self.get_db_user().id
        return {
            "id": self._db_id,
            "mail": self.mail,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "token": self.token,
            "can_create_event": self.can_create_event,
            "can_modify_all_events": self.can_modify_all_events,
            }
