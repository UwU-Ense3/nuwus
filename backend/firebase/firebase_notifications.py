import firebase_admin
from firebase_admin import credentials, messaging

from models import Event


def init() -> None:
    cred = credentials.Certificate(
        "firebase/nuwus-e3-firebase-key.json")  # fichier à récupérer dans les paramètres de firebase admin
    firebase_admin.initialize_app(cred)


def _send_notification(title: str, body: str, data: dict[str, object]) -> None:
    """
    Envoie une notification à tous les utilisateurs
    :param title: titre de la notification
    :param body: corps de la notification
    :param data: data de la notification
    """
    messaging.send(messaging.Message(topic='/topics/all',
                                     # token="dlwpNYDtQP-0HoYQWbf4Du:APA91bEhOJbRHG3MdkR4eZzGV6xSU5P1Z_2qarDRHXx-thEWnw02"
                                     #       "brdeNBUEWOKcC8yvlLPFefIn7drlWLhWfhng0RssDI6y-zJmk9LCYaKxTxNCg9Yt_K1G6n8OerQ"
                                     #       "OFZho9r21SveD",
                                     notification=messaging.Notification(title=title, body=body), data=data),
                   # dry_run=True  # todo : à mettre dans une variable d'environnement
                   )


def send_new_event_notification(new_event: Event, notification_text: str) -> None:
    """
    Envoie une notification push à tous les utilisateurs pour signaler un nouvel événement
    :param new_event: l'événement créé
    :param notification_text: le texte de la notification
    """
    _send_notification(title=new_event.name, body=notification_text, data={
        "event_id": str(new_event.id)
        })
