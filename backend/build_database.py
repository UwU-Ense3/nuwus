import datetime
import os
import sys

import sqlalchemy

from color_gen import generate_new_color
from config import db, db_file
from models import User, Event, Organizer, Tag
import dateutil.parser

# Data to initialize database with
users = [
    {
        "mail": "simardced31@gmail.com",
        "last_name": "Simard",
        "first_name": "Cédric",
    }
]

events = [
    {
        "begin_time": "2021-05-23T11:40:57.056Z",
        "description": "Description de l'événement\nsur plusieurs lignes.",
        "end_time": "2021-05-23T11:40:57.056Z",
        "location": "Greener",
        "name": "Evénement test",
        "organizers": [
            "BDE"
        ],
        "tags": ["Electro", "Fluo"],
        "image_link": "https://www.infonormandie.com/photo/art/grande/55114023-41340106.jpg?v=1617057766",
        "n_participants": 120,
        "price": 25
    }
]

# Create database file if it doesn't exist currently:
if not os.path.exists(db_file) or "--force" in sys.argv:
    if os.path.exists(db_file):
        os.remove(db_file)
    os.makedirs(os.path.dirname(db_file), exist_ok=True)

    # Create the database
    db.create_all()

    # iterate over the user structure and populate the database
    for user in users:
        new_user = User(mail=user.get("mail"),
                        last_name=user.get("last_name"),
                        first_name=user.get("first_name"))
        db.session.add(new_user)

    colors = []
    for event in events:
        new_event = Event(name=event.get("name"),
                          description=event.get("description"),
                          location=event.get("location"),
                          begin_time=dateutil.parser.isoparse(event.get("begin_time")),
                          end_time=dateutil.parser.isoparse(event.get("end_time")),
                          image_link=event.get("image_link"),
                          n_participants=event.get("n_participants"),
                          price=event.get("price")
                          )
        new_event.created_by = new_user
        new_event.updated_by = new_user
        new_event.updated_time = datetime.datetime.now()
        for org in event.get('organizers'):
            existing_organizer = Organizer.query.filter(Organizer.name == org).one_or_none()
            if existing_organizer is None:  # S'il n'existe pas
                new_event.organizers.append(Organizer(name=org))
            else:
                new_event.organizers.append(existing_organizer)
        for tag in event.get('tags'):
            existing_tag = Tag.query.filter(Tag.name == tag).one_or_none()
            if existing_tag is None:  # S'il n'existe pas
                color = generate_new_color(colors)
                new_event.tags.append(Tag(name=tag, color=color))
                colors.append(color)
            else:
                new_event.tags.append(existing_tag)
        db.session.add(new_event)
    db.session.commit()
    print("Database created!")

# if not number_views in events, then migrate the database: add a column number_views to events
else:
    try:
        db.engine.execute("SELECT number_views FROM event")
        print("Database already exists. Use --force to recreate it.")
    except sqlalchemy.exc.OperationalError as e:
        print("Migrating database...")
        db.engine.execute("ALTER TABLE event ADD COLUMN number_views INTEGER DEFAULT 0")
        db.session.commit()
        print("Database migrated!")
