"""
Implémentation des requêtes d'authentification sur les endpoints api/passwordless_login et api/register

Avant d'utiliser l'api, il faut renseigner l'API key (api_key=...), le client secret (client_secret=...) et l'application id (application_id=...)
dans un fichier secret.py.
ATTENTION A NE PAS COMMIT secret.key, IL CONTIENT DES DONNEES SENSIBLES !
secret.key est ajouté au fichier .gitignore, il ne devrait donc pas être commit, mais bien vérifier quand même
"""
from secrets import token_urlsafe
from typing import Optional

from flask import request
from fusionauth.fusionauth_client import FusionAuthClient

from config import db
from fusion_auth.fusion_auth_user import FusionAuthUser, Roles
from models import User
from secret import api_key, application_id

host_ip = "fusionauth"

client = FusionAuthClient(api_key, f"http://{host_ip}:9011")


def start_login(mail: str) -> tuple[dict, int]:  # todo: rename to email_address
    """
    Commence l'authentification passwordless en envoyant un mail à l'adresse spécifiée avec un code temporaire.
    Retourne un body vide et le status code de fusionauth
    """
    r_start = client.start_passwordless_login({
        "applicationId": application_id,
        "loginId": mail
        })

    # si l'utilisateur est déjà registered, le login se déroule normalement : on envoie le code par mail
    if r_start.response.status_code == 200:
        code = r_start.response.json()["code"]
        r_send = client.send_passwordless_code({
            "code": code
            })
        return {}, r_send.response.status_code

    # si l'utilisateur n'est pas registered (code 404 en réponse), on renvoie 404, ce qui signifie que l'utilisateur
    # doit d'abord s'enregistrer
    if r_start.response.status_code == 404:
        return {}, 404


def passwordless_login(code: str) -> tuple[dict, int]:
    """
    Termine l'authentification passwordless avec le code spécifié
    Retourne le json décrivant l'utilisateur que va recevoir l'appli et le status code de fusionauth
    """
    # Compte de test pour qu'apple puisse accéder à la page de création d'événement. Aucune autorisation n'est donnée
    if code == 'G9ewp&d*fYvmyEW9':
        return {
                   "id": 9999999,
                   "mail": "foo@2uwu4u.club",
                   "first_name": "foo",
                   "last_name": "foo",
                   "token": "G9ewp&d*fYvmyEW9",
                   "can_create_event": True,
                   "can_modify_all_events": True
                   }, 200

    r_login = client.passwordless_login({
        "code": code
        })
    fa_user = FusionAuthUser.from_json(r_login.response.json())

    # si l'utilisateur existe dans fusionauth mais n'est pas register dans l'application Nuwus sur fusionauth
    # (cas où l'utilisateur a été créé par fusionauth)
    if r_login.response.status_code == 202:
        # on ajoute la registration pour nuwus :

        r_registration = client.register(user_id=fa_user.fa_id, request={
            "generateAuthenticationToken": True,
            "registration": {
                "applicationId": application_id,
                "roles": ["default"]
                }
            })
        if r_registration.response.status_code != 200:
            return {
                       "message": "Erreur lors de la registration"
                       }, r_registration.response.status_code
        fa_user.patch_from_register_json(r_registration.response.json())

    # s'il y a une erreur, on renvoie le code d'erreur fusionauth
    if r_login.response.status_code not in [200, 202]:
        print("Erreur lors de la connexion : ", r_login.error_response)  # todo : ajouter module logging
        return {}, r_login.response.status_code

    # si aucun authentication token n'a été trouvé, on modifie la registration pour activer la génération de tokens
    if fa_user.token is None:
        r_registration = client.patch_registration(user_id=fa_user._db_id, request={
            "generateAuthenticationToken": True,
            "registration": {
                "applicationId": application_id,
                # "roles": ["default"]
                }
            })
        if r_registration.response.status_code != 200:
            print("Erreur lors de la registration : ", r_registration.error_response)
            return {
                       "message": "Erreur lors de la registration"
                       }, r_registration.response.status_code
        fa_user.patch_from_register_json(r_registration.response.json())

    return fa_user.to_json(), r_login.response.status_code


def register(user_json: dict) -> tuple[dict, int]:
    mail = user_json.get("mail")
    last_name = user_json.get("last_name")
    first_name = user_json.get("first_name")
    generated_password = token_urlsafe(12)
    r_register = client.register(request={
        "generateAuthenticationToken": True,
        "registration": {
            "applicationId": application_id,
            "roles": ["default"]
            },
        "user": {
            "email": mail,
            "lastName": last_name,
            "firstName": first_name,
            "password": generated_password
            }
        })
    if r_register.response.status_code != 200:
        print("Erreur lors de la création de l'utilisateur : ", r_register.error_response)
        # print(r_register.response.json())
        return {}, r_register.response.status_code
    return start_login(mail)


def _get_user_from_login_response(login_json):
    user = User(mail=login_json['user']['email'], first_name=login_json['user']['firstName'],
                last_name=login_json['user']['lastName'])
    existing_user = User.query.filter(User.mail == user.mail).one_or_none()
    if existing_user is None:
        db.session.add(user)
        db.session.commit()
        return User.query.filter(User.mail == user.mail).one()
    return existing_user


def _common_login(headers=None) -> Optional[FusionAuthUser]:
    """
    Fonction non exposée à l'api qui crée la requête pour se loguer avec Fusion Auth
    :param headers: doit contenir 'mail' et 'password'
    :return: Le json renvoyé par FusionAuth ou le code d'erreur si la requête à échoué
    """
    if headers is None:
        headers = request.headers
    if not ('mail' in headers and 'password' in headers):
        return None
    mail = headers["mail"]
    token = headers["password"]
    r_login = client.login(request={
        "applicationId": application_id,
        "loginId": mail,
        "noJWT": True,  # On n'utilise pas les JWT qui prennent du temps à générer
        "password": token
        })
    if r_login.response.status_code != 200:
        print("Erreur lors de la connexion : ", r_login.error_response)
        return None
    resp_json = r_login.response.json()
    fa_user = FusionAuthUser.from_json(resp_json)
    return fa_user


def login(headers=None) -> tuple[dict, int]:
    fa_user = _common_login(headers)
    if fa_user is None:
        return {
                   "message": "Erreur lors de la connexion"
                   }, 400
    json = fa_user.to_json()
    return json, 200


def get_roles_and_user(headers):
    """
    Renvoie la liste des rôles de l'utilisateur définis dans FusionAuth
    :param headers:
    :return:
    """
    fa_user = _common_login(headers)
    if fa_user is None:
        return [], None
    return fa_user.roles, fa_user.get_db_user()


def get_user_if_authenticated(headers):
    roles, user = get_roles_and_user(headers)
    if (Roles.DEFAULT in roles) or (Roles.MANAGER in roles) or (Roles.ADMIN in roles):
        return user
    return None


def get_user_if_manager(headers):
    roles, user = get_roles_and_user(headers)
    if (Roles.MANAGER in roles) or (Roles.ADMIN in roles):
        return user
    return None


def get_user_if_admin(headers):
    roles, user = get_roles_and_user(headers)
    if Roles.ADMIN in roles:
        return user
    return None
