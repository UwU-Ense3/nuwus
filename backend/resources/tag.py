from flask import abort, request

from color_gen import generate_new_color
from config import db
from models import Tag, TagSchema
from resources.auth import get_user_if_manager


def read_all():
    """
    La fonction met en forme la réponse de la
    requête sur api/tag

    :return: json string de la table des tags
    """
    # On récupère les tags de notre table
    user = Tag.query.order_by(Tag.name).all()

    # Serialize de la table en json
    tag_schema = TagSchema(many=True)
    data = tag_schema.dump(user)
    return data


def read_one(tag_id):
    """
    retourne l'organisateur avec l'id correspondante
    :param tag_id:  id de l'organisateur
    :return:    détails de l'organisateur correspondant
    """
    tag = Tag.query.filter(Tag.id == tag_id).one_or_none()
    if tag is not None:
        tag_schema = TagSchema()
        data = tag_schema.dump(tag)
        return data
    else:
        abort(
            404,
            "Organisateur non trouvé pour l'id " + str(tag_id)
        )


def create(name):
    if get_user_if_manager(request.headers) is None:
        return 'Unauthorized', 401
    all_tags = Tag.query.all()
    existing_colors = [tag.color for tag in all_tags]
    color = generate_new_color(existing_colors)
    tag = Tag(name=name, color=color)
    db.session.add(tag)
    db.session.commit()
    return TagSchema().dump(tag), 201


def update(tag_id, name, color):
    if get_user_if_manager(request.headers) is None:
        return 'Unauthorized', 401
    tag = Tag.query.filter(Tag.id == tag_id).one_or_none()
    if tag is None:
        abort(
            404,
            "Tag non trouvé pour l'id " + str(tag_id)
        )
    new_tag = Tag(name=name, id=tag_id, color=color)
    db.session.merge(new_tag)
    db.session.commit()
    return TagSchema().dump(tag), 201


def delete(tag_id):
    if get_user_if_manager(request.headers) is None:
        return 'Unauthorized', 401
    """
    Suppression d'un tag de la base de données
    :param tag_id:  id du tag à supprimer
    :return: 204 succès, 404 introuvable
    """

    tag = Tag.query.filter(Tag.id == tag_id).one_or_none()
    if tag is None:
        abort(404, "Le tag pour l'id ", tag_id, " n'existe pas")
    db.session.delete(tag)
    db.session.commit()
    return "", 204


