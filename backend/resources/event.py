from datetime import datetime

from flask import abort, request

from config import db
from firebase.firebase_notifications import send_new_event_notification
from models import Event, EventSchema, Organizer, Tag
from resources import tag
from resources.auth import get_user_if_manager, _common_login


def _event_from_json(event_json):
    """
    Création d'un événement à partir d'un json
    :param event_json: json de l'événement
    :return: l'événement créé
    """
    # Création du schéma pour deserialiser le json event
    schema = EventSchema()

    # Recuperation des organisateurs (liste de noms)
    organizers_names = event_json["organizers"]
    del event_json["organizers"]  # On le supprime ensuite du json pour ne pas perturber la deserialisation

    tags_name = event_json.get('tags')
    del event_json["tags"]

    # Désérialisation
    new_event: Event = schema.load(event_json, session=db.session, )

    if new_event.begin_time > new_event.end_time:
        abort(404, "La fin de l'événement ne peut pas être avant son début", )

    # Pour chaque organisateur dans la liste des noms des organisateurs
    for organizer_name in organizers_names:
        # Trouve l'organisateur, le crée s'il n'existe pas deja
        existing_organizer = Organizer.query.filter(Organizer.name == organizer_name).one_or_none()
        if existing_organizer is None:  # S'il n'existe pas
            new_organizer = Organizer(name=organizer_name)
            new_event.organizers.append(new_organizer)
            db.session.add(new_organizer)
        else:  # S'il existe déjà
            new_event.organizers.append(existing_organizer)

    # Même chose pour les tags
    for tag_name in tags_name:
        # Trouve l'organisateur, le crée s'il n'existe pas deja
        existing_tag = Tag.query.filter(Tag.name == tag_name).one_or_none()
        if existing_tag is None:  # S'il n'existe pas
            tag.create(name=tag_name)
            new_tag = Tag.query.filter(Tag.name == tag_name).one_or_none()
            new_event.tags.append(new_tag)
            db.session.add(new_tag)
        else:  # S'il existe déjà
            new_event.tags.append(existing_tag)
    return new_event


def read_all():
    """
    La fonction met en forme la réponse de la
    requête sur api/event

    :return: json string de la table des events
    """

    # if not is_authenticated(request.headers):
    #     return 'Unauthorized', 401

    hide_past = request.headers.get('hide_past') == 'true'
    # On récupère les évenements de notre table
    if hide_past:
        events = Event.query.filter(Event.end_time >= datetime.now()).order_by(Event.begin_time).all()
    else:
        events = Event.query.order_by(Event.begin_time).all()
    # Serialize de la table en json
    event_schema = EventSchema(many=True)
    data = event_schema.dump(events)
    return data



def read_one(event_id):
    """
    retourne l'évenement avec l'id correspondant
    :param event_id:  id de l'événement
    :return:    détails de l'événement correspondant
    """
    # if not is_authenticated(request.headers):
    #     return 'Unauthorized', 401

    event = Event.query.filter(Event.id == event_id).one_or_none()

    if event is not None:
        event_schema = EventSchema()
        data = event_schema.dump(event)
        # increment number of views
        if event.number_views is None:
            event.number_views = 0
        event.number_views += 1
        db.session.commit()
        return data
    else:
        abort(404, "Evénement non trouvé pour l'id " + str(event_id))


def create(event_json):
    user_origin = get_user_if_manager(request.headers)
    if user_origin is None:
        return 'Unauthorized', 401

    send_notification = event_json.get("send_notification")
    event_json.pop("send_notification", None)
    notification_text = event_json.get("notification_text")
    event_json.pop("notification_text", None)

    new_event = _event_from_json(event_json)
    new_event.created_by = user_origin
    new_event.create_time = datetime.now()
    new_event.updated_by = user_origin
    new_event.update_time = datetime.now()

    db.session.add(new_event)
    db.session.commit()

    schema = EventSchema()
    response = schema.dump(new_event)
    if send_notification is None:
        send_notification = True
    if send_notification:
        if notification_text is None or notification_text == "":
            notification_text = new_event.description
        try:
            send_new_event_notification(new_event, notification_text)
        except Exception as e:
            print(e)

    return response, 201


def update(event_id, event_json):
    existing_event = Event.query.filter(Event.id == event_id).one_or_none()
    fa_user = _common_login(request.headers)
    if fa_user is None or not ((
                                       fa_user.can_create_event and fa_user.get_id() == existing_event.created_by_id) or fa_user.can_modify_all_events):
        return 'Unauthorized', 401
    if existing_event is None:
        abort(404, "L'événement pour l'id ", event_id, " n'existe pas")

    # On reset les organisateurs et les tags pour pouvoir append les nouveaux ensuite
    existing_event.organizers = []
    existing_event.tags = []

    event_json["id"] = event_id
    send_notification = event_json.get("send_notification")
    event_json.pop("send_notification", None)
    notification_text = event_json.get("notification_text")
    event_json.pop("notification_text", None)

    new_event = _event_from_json(event_json)
    new_event.updated_by = fa_user.get_db_user()
    new_event.update_time = datetime.now()

    db.session.merge(new_event)
    db.session.commit()

    schema = EventSchema()
    response = schema.dump(new_event)
    if send_notification is None:
        send_notification = False
    if send_notification:
        if notification_text is None or notification_text == "":
            notification_text = "Événement modifié"
        try:
            send_new_event_notification(new_event, notification_text)
        except Exception as e:
            print(e)
    return response, 201


def delete(event_id):
    """
    Suppression d'un événement de la base de données
    :param event_id:  id de l'événement à supprimer
    :return: 204 succès, 404 introuvable
    """

    existing_event = Event.query.filter(Event.id == event_id).one_or_none()
    fa_user = _common_login(request.headers)
    if fa_user is None or not ((
                                       fa_user.can_create_event and fa_user.get_id() == existing_event.created_by_id) or fa_user.can_modify_all_events):
        return 'Unauthorized', 401
    if existing_event is None:
        abort(404, "L'événement pour l'id ", event_id, " n'existe pas")
    db.session.delete(existing_event)
    db.session.commit()
    return "", 204
