"""
Ce module est l'implémentation des requêtes pour l'API REST. 
"""
from flask import make_response, abort, request

from config import db
from models import User, UserSchema
from resources.auth import get_user_if_admin, get_user_if_manager

BYTES_API_KEY = 32


def read_all():
    """
    La fonction met en forme la réponse de la 
    requête sur api/user

    :return:        json string de la table des user
    """
    if get_user_if_admin(request.headers) is None:
        return 'Unauthorized', 401
    # On va récupérer les utilisateurs dans notre table
    user = User.query.order_by(User.last_name).all()

    # Serialize de la table en json
    user_schema = UserSchema(many=True)
    data = user_schema.dump(user)
    return data


def read_one(id):
    """
    Cette fonction donne la réponse à la requête 
    /api/user/{user_key}, qui est l'utilisateur avec
    l'API Key user_key

    :param user_key:   API Key de l'utilisateur à aller chercher
    :return:           L'utilisateur avec l'API Key correspondante
    """
    if get_user_if_admin(request.headers) is None:
        return 'Unauthorized', 401

    # Récupération de l'utilisateur
    user = User.query.filter(User.id == id).one_or_none()

    # Est-ce que l'utilisateur existe ? 
    if user is None:
        abort(404, f"Utilisateur non trouvé pour l'id {id}")
    # Serialize l'utilisateur récupéré depuis la base de donnée
    user_schema = UserSchema()
    data = user_schema.dump(user)
    return data


def create(user_json):
    """
    crée un nouvel utilisateur dans la table des user

    :param user:  user qu'il faut créer
    :return:        201 succès, 406 existe déjà
    """
    if get_user_if_admin(request.headers) is None:
        return 'Unauthorized', 401
    user = _user_from_json(user_json)
    existing_user = _get_existing_user(user)

    # Peut-on ajouter cet user ?
    if existing_user is not None:
        abort(
            409,
            "L'utilisateur {first_name} {last_name} avec l'adresse e-mail {email} existe déjà".format(
                email=user.mail, first_name=user.first_name, last_name=user.last_name
            ),
        )

    new_user = User(email=user.mail, last_name=user.last_name, first_name=user.first_name)
    # Ajout de l'user à la table user
    db.session.add(new_user)
    db.session.commit()

    # Serialize et retourne le nouvel utilisateur
    user_schema = UserSchema()
    data = user_schema.dump(new_user)

    return data, 201


def update(id, user_json):
    """
    Cette fonction met à jour un utilisateur dans la base de données
    On envoie une erreur si l'utilisateur n'existe ou pas, et dans le cas
    où un autre user à le même mail dans la DB.

    :param id:    id de l'user à mettre à jour
    :param user_json:        user à mettre à jour
    :return:            Le nouvel user mis à jour
    """
    if get_user_if_admin(request.headers) is None:
        return 'Unauthorized', 401

    # On récupère l'utilisateur dans la table user qui possède la clé user_key
    update_user = User.query.filter(
        User.id == id
    ).one_or_none()

    user = _user_from_json(user_json)
    existing_user = _get_existing_user(user)

    # Est-ce que l'utilisateur avec l'API de la requête 
    # existe
    if update_user is None:
        abort(
            404,
            "Il n'y pas d'utilisateur pour l'API Key: {user_key}".format(user_key=id),
        )

    # Est-ce que l'utilisateur mis à jour va faire un doublon avec un autre utilisateur ? 
    elif existing_user is not None and existing_user._db_id != id:
        abort(409,
              "L'utilisateur {first_name} {last_name} avec l'adresse e-mail {email} existe déjà".format(email=user.mail,
                                                                                                        first_name=user.first_name,
                                                                                                        last_name=user.last_name), )

    # Sinon, on peut faire la mise à jour
    # On définit l'API Key de l'utilisateur
    user['id'] = id
    # On transforme le json en objet à donner à manger à la db
    schema = UserSchema()
    updated_user = schema.load(user, session=db.session)

    # On merge dans la base de donnée la mise à jour
    db.session.merge(updated_user)
    db.session.commit()

    # Retourne l'utilisateur mis à jour
    data = schema.dump(update_user)

    return data, 200


def delete(id):
    """
    Supression d'un utilisateur de la base de donnée

    :param id: id de l'utilisateur à supprimer
    :return:            200 succès, 404 introuvable
    """
    if get_user_if_admin(request.headers) is None:
        return 'Unauthorized', 401

    # Obtention de l'utilisateur 
    user = User.query.filter(User.id == id).one_or_none()

    # A-t-on trouvé l'utilisateur ?
    if user is not None:
        db.session.delete(user)
        db.session.commit()
        return make_response(
            "L'utilisateur avec l'API {id} a été supprimé".format(id=id), 200
        )

    # Sinon l'utilisateur n'a pas été trouvé encore
    else:
        abort(
            404,
            "Aucun utilisateur trouvé pour cette API Key: {id}".format(id=id),
        )


def create_event_permission():
    """
    Check si l'utilisateur peut créer un évenement
    """
    user_origin = get_user_if_manager(request.headers)
    if user_origin is None:
        return 'Unauthorized', 401

    return 200


def _user_from_json(user_json):
    """
    Renvoie un objet User à partir d'un json

    :param user_json:   json de l'utilisateur
    :return:            l'utilisateur
    """
    return UserSchema().load(user_json, session=db.session)


def _get_existing_user(user):
    """
    Cette fonction n'est pas exposée à l'api, elle sert simplement à vérifier si l'utilisateur spécifié existe
    déjà ou non, et à renvoyer l'utilisateur existant s'il existe, ou none s'il n'existe pas
    :param user:
    :return:
    """
    return User.query.filter(User.mail == user.mail) \
        .filter(User.last_name == user.last_name) \
        .filter(User.first_name == user.first_name) \
        .one_or_none()
