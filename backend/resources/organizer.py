from flask import abort, request

from config import db
from models import Organizer, OrganizerSchema
from resources.auth import get_user_if_manager


def read_all():
    """
    La fonction met en forme la réponse de la
    requête sur api/organizer

    :return: json string de la table des organizer
    """
    user = Organizer.query.order_by(Organizer.name).all()

    # Serialize de la table en json
    event_schema = OrganizerSchema(many=True)
    data = event_schema.dump(user)
    return data


def read_one(organizer_id):
    """
    retourne l'organisateur avec l'id correspondante
    :param organizer_id:  id de l'organisateur
    :return:    détails de l'organisateur correspondant
    """
    organizer = Organizer.query.filter(Organizer.id == organizer_id).one_or_none()
    if organizer is not None:
        organizer_schema = OrganizerSchema()
        data = organizer_schema.dump(organizer)
        return data
    else:
        abort(
            404,
            "Organisateur non trouvé pour l'id " + str(organizer_id)
        )


def create(name, color):
    if get_user_if_manager(request.headers) is None:
        return 'Unauthorized', 401
    organizer = Organizer(name=name, color=color)
    db.session.add(organizer)
    db.session.commit()
    return OrganizerSchema().dump(organizer), 201


def update(organizer_id, name, color):
    if get_user_if_manager(request.headers) is None:
        return 'Unauthorized', 401
    organizer = Organizer.query.filter(Organizer.id == organizer_id).one_or_none()
    if organizer is None:
        abort(
            404,
            "Organisateur non trouvé pour l'id " + str(organizer_id)
        )
    new_organizer = Organizer(name=name, id=organizer_id, color=color)
    db.session.merge(new_organizer)
    db.session.commit()
    return OrganizerSchema().dump(organizer), 201


def delete(organizer_id):
    if get_user_if_manager(request.headers) is None:
        return 'Unauthorized', 401
    """
    Suppression d'un événement de la base de données
    :param event_id:  id de l'événement à supprimer
    :return: 204 succès, 404 introuvable
    """

    organizer = Organizer.query.filter(Organizer.id == organizer_id).one_or_none()
    if organizer is None:
        abort(404, "L'événement pour l'id ", organizer_id, " n'existe pas")
    db.session.delete(organizer)
    db.session.commit()
    return "", 204


