connexion[swagger-ui]~=2.8.0
Flask~=1.1.4
Flask-SQLAlchemy == 2.5.1
SQLAlchemy == 1.4
flask-marshmallow == 0.14.0
marshmallow-sqlalchemy == 0.26.1
marshmallow == 3.12.2
DateTime==4.3
python-dateutil==2.8.2
fusionauth-client==1.36.0
Flask-Cors ~= 3.0.10
waitress~=2.0.0
firebase-admin==5.2.0
markupsafe==2.0.1