# NUwUs

![Play store bannerbanner](readme.assets/play_store_banner.png)

[<img alt="Disponible sur Google Play" src="readme.assets/play-store-badge.svg" height="50" />](https://play.google.com/store/apps/details?id=com.uwu.nuwus&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1)[<img alt="Disponible sur l'App Store" src="readme.assets/app-store-badge.svg" height="50" />](https://apps.apple.com/us/app/nuwus/id1627563358)

## L'application

Le but final est d'avoir un endroit autre que Facebook où on peut voir tous les événements organisés à l'e3 par toutes les assos, tous les bureaux et autres. L'avantage par rapport à fb est que c'est plus facile pour avoir une vision d'ensemble de tous les événements, on pourra trier par ordre chronologique et on n'a pas besoin de compte Facebook.

Pour l'instant on a une appli mobile fonctionnelle qui permet d'afficher une liste d'événements stockée dans une base de donnée sur un serveur. C'est à dire que la structure de base de l'appli et du backend (le code sur le serveur) fonctionnent, maintenant il faut compléter pour ajouter les fonctionnalités utiles, rendre l'application robuste, etc.

## Contribuer

Si tu souhaites contribuer au développement de l'application, c'est très simple. Il faut tout d'abord aller voir quoi faire dans [la feuille de route du repo](https://gitlab.com/UwU-Ense3/nuwus/-/boards/2785528). Ensuite, suis [ce guide](https://gitlab.com/UwU-Ense3/nuwus/-/wikis/contribuer/) pour commencer à apporter tes modifications.

## Des questions ?

Viens parler de NUwUs sur [notre discord](https://discord.gg/BmTSZug) !

## Wiki

Tu pourras trouver des guides, des tutos et des ressources sur [le wiki du projet](https://gitlab.com/UwU-Ense3/nuwus/-/wikis/Documentation).

## License

L'application est sous license GPLv3.

## Copyright

Apple and Apple Logo are trademarks of Apple Inc.

Google Play et le logo Google Play sont des marques de Google LLC.
